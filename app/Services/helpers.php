<?php

// its for global helpers
function actmenu($path, $class = 'current', $segment = false)
{
    $curr_path = $segment !== false? request()->segment($segment) : request()->path();

    if (is_array($path)) :

        foreach ($path as $data) :
            if ($curr_path == $data)
                return $class;
        endforeach;

    else :

        if ($curr_path == $path)
            return $class;

    endif;

    return '';
}
