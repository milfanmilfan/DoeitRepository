<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Article;
use App\ArticleTag;

class HomeController extends Controller
{
	public function article_detail($slug, Request $req)
	{
		$data = Article::where('slug', $slug)->first();

		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', $data->id_author)
			->first();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$status = 1;

		if(empty(auth()->user()->profile_member->id ))
		{
			$status = 0;
		}

		if (! $data)
			return redirect('/');

		if ($req->method() == 'POST') :

			$rules = [
				'comment' => 'required|min:5',
				'id_article' => 'required|integer',
			];
			$validator = Validator::make($req->all(), $rules);

			if ($validator->fails())
				return redirect()->back()->withErrors($validator);

			ArticleComment::create($req->all());
			return redirect()->back();
		endif;

		return view('layouts.member.pages.dashboard.blog_detail', compact('data', 'user', 'status', 'already_paid'));
	}

    public function home()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.home', compact('already_paid'));
	}

	public function feed()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.feed', compact('already_paid'));
	}

	public function latest()
	{
		$article = Article::with('author')->withCount('article_comment')->whereIn('article_type', function ($query) {
				$query->select('interest_value')->from('interest_user')
					->Where('interest_user.id_user','=',auth()->user()->id);
			})->orderBy('id', 'desc')->take(40)->get();

		$author = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.latest', compact('author', 'article', 'already_paid'));
	}

	public function articles()
	{
		$article = Article::with('author')->withCount('article_comment')->whereIn('article_type', function ($query) {
			$query->select('interest_value')->from('interest_user')
				->Where('interest_user.id_user','=',auth()->user()->id);
		})->orderBy('id', 'desc')->get();

		$author = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.articles', compact('author', 'article', 'already_paid'));
	}

	public function news()
	{
		$article = Article::with('author')->withCount('article_comment')->whereIn('article_type', function ($query) {
			$query->select('interest_value')->from('interest_user')
				->Where('interest_user.id_user','=',auth()->user()->id);
		})->orderBy('id', 'desc')->get();

		$author = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.news', compact('author', 'article', 'already_paid'));
	}

	public function data()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.data', compact('already_paid'));
	}

	public function alert()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.alert', compact('already_paid'));
	}

	public function stock_talks()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.stock_talks', compact('already_paid'));
	}

	public function became_author()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		return view('layouts.member.pages.dashboard.became_author', compact('already_paid'));
	}

	public function paid_articles()
	{
		$title = 'All Article';

		$articles = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->get();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$all_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->get()->count();

		$market_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%1%")
			->get()->count();

		$marketoutlook_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%1%")
			->get()->count();

		$stockideas_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%3%")
			->get()->count();

		$irpresentation_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%6%")
			->get()->count();

		$broker_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%4%")
			->get()->count();

		$funds_count = DB::table('articles')
			->join('marketplace', 'marketplace.id_article', '=', 'articles.id')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'marketplace.id_user')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->where('marketplace.status', '=', 1)
			->where('article_type', 'LIKE', "%5%")
			->get()->count();

		return view('layouts.member.pages.dashboard.paid_articles', compact('already_paid', 'articles', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

	public function article_detail_paid($slug, Request $req)
	{
		$data = Article::where('slug', $slug)->first();

		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', $data->id_author)
			->first();

		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$status = 1;

		if(empty(auth()->user()->profile_member->id ))
		{
			$status = 0;
		}

		if (! $data)
			return redirect('/');

		if ($req->method() == 'POST') :

			$rules = [
				'comment' => 'required|min:5',
				'id_article' => 'required|integer',
			];
			$validator = Validator::make($req->all(), $rules);

			if ($validator->fails())
				return redirect()->back()->withErrors($validator);

			ArticleComment::create($req->all());
			return redirect()->back();
		endif;

		return view('layouts.member.pages.dashboard.blog_detail', compact('data', 'user', 'status', 'already_paid'));
	}


}
