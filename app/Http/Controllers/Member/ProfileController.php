<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
	public function profile()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$user = DB::table('member_profiles')
			->select('member_profiles.*')
			->where('member_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.member.pages.dashboard.profile', compact('user', 'already_paid'));
	}

	public function followers()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$user = DB::table('member_profiles')
			->select('member_profiles.*')
			->where('member_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.member.pages.dashboard.profile_follower', compact('user', 'already_paid'));
	}

	public function following()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$user = DB::table('member_profiles')
			->select('member_profiles.*')
			->where('member_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.member.pages.dashboard.profile_following', compact('user', 'already_paid'));
	}

	public function comments()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$user = DB::table('member_profiles')
			->select('member_profiles.*')
			->where('member_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.member.pages.dashboard.profile_comments', compact('user', 'already_paid'));
	}

	public function stocktalks()
	{
		$already_paid = DB::table('marketplace')
			->select('marketplace.*')
			->where('marketplace.id_user', '=', auth()->user()->id)
			->get()->count();

		$user = DB::table('member_profiles')
			->select('member_profiles.*')
			->where('member_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.member.pages.dashboard.profile_stocktalks', compact('user', 'already_paid'));
	}

	public function edit($id)
	{
		dd($id);
	}
}
