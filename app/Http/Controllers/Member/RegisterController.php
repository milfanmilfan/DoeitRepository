<?php

namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\VerifyAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class RegisterController extends Controller
{
	public function member_register(Request $req, $token)
	{
		if (! $token)
		{
			return redirect('/');
		}

		$check = User::where('token', $token)->first();

		if (! $check)
			return redirect('/');

		if ($req->method() == 'POST') :


			DB::beginTransaction();
			try
			{
				Validator::make($req->all(), [
					'first_name'       	=> 'required',
					'last_name'        	=> 'required',
					'news'	    		=> 'required',
					'category'        	=> 'required'
				])->validate();

				$check->name = $req->first_name.' '.$req->last_name;
				$check->token = null;
				$check->complete = 1;

				$check->save();

				$category = $req->input('category');
				$newscategory = $req->input('newscategory');
				$broker = $req->input('broker');
				$platform = $req->input('platforms');

				$primary_broker = $req->input('primarybrok');
				$primary_platform = $req->input('platformbrok');
				$stocks = $req->input('stocks');

				$is_investor = "";
				if($req->input('accredited') == 1)
				{
					$is_investor = 1;
				}
				else
				{
					$is_investor = 0;
				}

				$has_broker = "";
				if(!empty($req->input('has_broker')))
				{
					$has_broker = 0;
				}
				else
				{
					$has_broker = 1;
				}

				$has_platform = "";
				if(!empty($req->input('has_platform')))
				{
					$has_platform = 0;
				}
				else
				{
					$has_platform = 1;
				}

				$alerts = "";
				if(!empty($req->input('alerts')))
				{
					$alerts = 1;
				}
				else
				{
					$alerts = 0;
				}

				$member_profiles = DB::table('member_profiles')->insert([
					'id_user'          	=> $check->id,
					'first_name'       	=> $req->input('first_name'),
					'last_name'        	=> $req->input('last_name'),
					'alert'        		=> $alerts,
					'is_investor'  		=> $is_investor,
					'aum'		  		=> $req->input('valueaum'),
					'news_type'		  	=> $req->input('news'),
					'has_broker'		=> $has_broker,
					'has_platform'		=> $has_platform,
					'primary_broker'	=> $primary_broker[0],
					'primary_platform'	=> $primary_platform[0],
					'created_at'       	=> Carbon::now()
				]);

				if (!empty($category))
				{
					foreach($category as $key => $value)
					{
						$db_category = DB::table('interest_user')->insert([
							'id_user'        => $check->id,
							'interest_value' => $value,
							'created_at'     => Carbon::now()
						]);
					}
				}

				if (!empty($newscategory))
				{
					foreach($newscategory as $key => $value)
					{
						$db_stocks = DB::table('members_subscriber')->insert([
							'id_user'        => $check->id,
							'interest_value' => $value,
							'created_at'     => Carbon::now()
						]);
					}
				}

				if (!empty($profesional_platform))
				{
					foreach($profesional_platform as $key => $value)
					{
						$db_stocks = DB::table('members_subscriber')->insert([
							'id_user'        => $check->id,
							'interest_value' => $value,
							'created_at'     => Carbon::now()
						]);
					}
				}

				if (!empty($broker))
				{
					foreach($broker as $key => $value)
					{
						$db_stocks = DB::table('members_broker')->insert([
							'id_user'       => $check->id,
							'broker_value' 	=> $value,
							'status' 		=> "broker",
							'created_at'    => Carbon::now()
						]);
					}
				}

				if (!empty($platform))
				{
					foreach($platform as $key => $value)
					{
						$db_stocks = DB::table('members_broker')->insert([
							'id_user'       => $check->id,
							'broker_value' 	=> $value,
							'status' 		=> "platform",
							'created_at'    => Carbon::now()
						]);
					}
				}

				if (!empty($platform))
				{
					foreach($platform as $key => $value)
					{
						$db_stocks = DB::table('members_broker')->insert([
							'id_user'       => $check->id,
							'broker_value' 	=> $value,
							'status' 		=> "platform",
							'created_at'    => Carbon::now()
						]);
					}
				}

				if (!empty($stocks))
				{
					foreach($stocks as $key => $value)
					{
						$db_stocks = DB::table('member_stocks')->insert([
							'id_user'       => $check->id,
							'stocks_value' 	=> $value,
							'created_at'    => Carbon::now()
						]);
					}
				}
			}
			catch (\Exception $e)
			{
				DB::rollback();
				throw $e;
				return redirect()->route('member.register', $token);
			}

			DB::commit();

			$req->session()->flash('success', 'Author account activation success');
			return redirect()->route('author.login');

		endif;

		return view('layouts.member.pages.registration.member_register', compact('token'));
	}

	public function mail_register(Request $req)
	{
		$validator = JsValidator::make([
			'email'    => 'required|email',
			'password' => 'required|min:6'
		],  [], [], '#form-register');

		if ($req->method() == 'POST')
		{
			$token = str_random(30);
			$email = $req->get('email');

			$user = DB::table('users')
				->select('users.*')
				->where('users.email', '=', $req->get('email'))
				->first();

			if(!empty($user))
			{
				$req->session()->flash('success', 'Your Email Already Registered');
				return redirect()->route('member.mail.register');
			}
			else
			{
				DB::beginTransaction();
				try
				{

					$users = DB::table('users')->insert([
						'email'      => $req->input('email'),
						'token'      => $token,
						'password'   => bcrypt($req->password),
						'author'     => 0,
						'created_at' => Carbon::now()
					]);

					$link = 'member/register';

					Mail::to($email)->send(new VerifyAccount($email, $token, $link));

				}
				catch (\Exception $e)
				{
					DB::rollback();
					throw $e;
					return redirect('member/register/'.$token);
				}

				DB::commit();
				if($users)
				{
					return redirect('member/register/welcome/'.$token);
				}
				else
				{
					return redirect('member/register/'.$token);
				}
			}
		}

		return view('layouts.member.pages.registration.mail_register', compact('validator'));
	}

	public function welcome($token)
	{
		$user = DB::table('users')
			->select('users.*')
			->where('users.token', '=', $token)
			->first();
		if(!empty($user))
		{
			return view('layouts.member.pages.registration.greeting_register');
		}
		else
		{
			return redirect('/');
		}
	}
}
