<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleCommentController extends Controller
{
    public function index()
	{
		return view('layouts.author.pages.dashboard.manage_article_comments');
	}
}
