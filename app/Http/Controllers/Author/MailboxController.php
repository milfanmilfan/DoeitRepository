<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MailboxController extends Controller
{
	public function index()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.author.pages.dashboard.mailbox', compact('user'));
	}
}
