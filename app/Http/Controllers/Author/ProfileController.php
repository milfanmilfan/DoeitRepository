<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use JsValidator;

class ProfileController extends Controller
{
	public function index()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		return view('layouts.author.pages.dashboard.profile',compact('user'));
	}

	public function ShowInterest(Request $req)
	{
		$data = DB::table('interest_user')
			->select('interest_user.interest_value')
			->where('interest_user.id_user', '=', auth()->user()->id)
			->get();
		
		if($req->method() == 'PATCH')
		{
			$v = Validator::make($req->all(), [
				"category" => 'required'
			])->validate();

			DB::beginTransaction();
			try
			{
				if($req->has_change == 1)
				{
					DB::table('interest_user')
						->where('interest_user.id_user', '=', auth()->user()->id)
						->delete();

					foreach($req->category as $key => $value)
					{
						$db_category = DB::table('interest_user')->insert([
							'id_user'        => auth()->user()->id,
							'interest_value' => $value,
							'created_at'     => Carbon::now()
						]);
					}
				}
			}
			catch(\Exception $e)
			{
				DB::rollback();
				throw $e;
				$req->session()->flash('error', $e->getMessage());
				return redirect()->route('author.profileinterest');
			}
			DB::commit();
			
			$req->session()->flash('success', 'Your Interest has Updated');
			return redirect()->route('author.profileinterest');
		}

		return view('layouts.author.pages.dashboard.profile_interest',compact('data'));
	}

	public function ShowPersonalDetail(Request $req)
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		if($req->method() == 'PATCH')
		{
			$v = Validator::make($req->all(), [
				"profile_pict" => 'image',
				"datebirth"    => 'required',
				"firstname"    => 'required',
				"lastname"     => 'required',
				"minibio"      => 'required',
				"address"      => 'required',
				"city"         => 'required',
				"postalcode"   => 'required|numeric',
				"phone"        => 'required|numeric',
				"sitename"     => 'required',
				"siteurl"      => 'required',
				"facebook"     => 'nullable',
				"twitter"      => 'nullable',
				"skype"        => 'nullable',
				"linkedin"     => 'nullable'
			])->validate();

			DB::beginTransaction();
			try
			{
				if($req->hasFile('profile_pict'))
				{
					$imageName = time().'.'.$req->file('profile_pict')->getClientOriginalExtension();
					$req->file('profile_pict')->move(public_path('web/img/profile'), $imageName);
				}
				else
				{
					$imageName = $user->profile_pictures;
				}

				DB::table('author_profiles')
					->where('author_profiles.id_user', '=', auth()->user()->id)
					->update([
						'profile_pictures' => $imageName,
						'first_name' => $req->firstname,
						'last_name' => $req->lastname,
						'phone_number' => $req->phone,
						'dob' => Carbon::createFromFormat('d F Y', $req->datebirth),
						'address' => $req->address,
						'postal_code' => $req->postalcode,
						'city' => $req->city,
						'site_name' => $req->sitename,
						'site_url' => $req->siteurl,
						'bio' => $req->minibio,
						'facebook' => $req->facebook,
						'twitter' => $req->twitter,
						'skype' => $req->skype,
						'linkedin' => $req->linkedin,
					]);

				$user = auth()->user();
				$user->name = $req->firstname.' '.$req->lastname;
				$user->save();
			}
			catch(\Exception $e)
			{
				DB::rollback();
				throw $e;
				$req->session()->flash('error', $e->getMessage());
				return redirect()->route('author.profilepersonal');
			}
			DB::commit();

			$req->session()->flash('success', 'Your Profile has been Updated');
			return redirect()->route('author.profilepersonal');
		}

		return view('layouts.author.pages.dashboard.profile_detail',compact('user','validator'));
	}

	public function profiles($id)
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', $id)
			->first();

		$article = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.*')
			->where('articles.id_author', '=', $id)
			->get();

		return view('layouts.front.pages.author_profiles', compact('article', 'user'));
	}
}
