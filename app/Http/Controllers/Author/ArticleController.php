<?php

namespace App\Http\Controllers\Author;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Article;
use App\ArticleTag;
use App\ArticleTicker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
	public function write_article(Request $req)
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		if ($req->method() == 'POST')
		{
			DB::beginTransaction();
			try {

				$file = null;
				if ($req->hasFile('file')) :
					if (! $req->file('file')->isValid())
						return redirect()->back();
					$cover = $req->file('file');
					$file = 'file_'.md5(microtime(true)).'.'.$cover->extension();
					$cover->move(public_path('upload/article'), $file);
				endif;

				$req['file'] = $file;

				$price = 0;
				if(!empty($req->input('price')))
				{
					$price = $req->input('price');
				}

				str_slug("adad");

				$article = DB::table('articles')->insertGetId([
					'article_category'  => $req->input('article_category'),
					'article_type' 		=> $req->input('article_type'),
					'title' 			=> $req->input('title'),
					'slug' 				=> str_replace(" ","-",$req->input('title')),
					'description' 		=> $req->input('description'),
					'file' 				=> $file,
					'article' 			=> $req->input('article'),
					'is_paid' 			=> $req->input('article_category'),
					'price' 			=> 0,
					'status' 			=> "publish",
					'id_author' 		=> auth()->user()->id,
					'created_at'   		=> Carbon::now()
				]);

				$tag = $req->input('tag');

				$data = [];
				if (!empty($tag))
				{
					foreach($tag as $value)
					{
						$data[] = [
								'id_article'    => $article,
								'id_tag'    	=> $value,
								'created_at'   	=> Carbon::now()
							];
					}

					DB::table('article_tags')->insert($data);
				}

			} catch (\Exception $e) {
				DB::rollback();
 				$req->session()->flash('status', 'Fail to create article. Try Again');
				return redirect()->back();
			}
			DB::commit();

			return redirect('authors/manage-article');
		}
		else
		{
			return view('layouts.author.pages.dashboard.write_article', compact('user'));
		}
	}

	public function upload_image(Request $req)
	{
		$CKEditor = $req->get('CKEditor');
	    $funcNum = $req->get('CKEditorFuncNum');
	    $message = $url = '';
	    if ($req->hasFile('upload')) {
	        $file = $req->file('upload');
	        if ($file->isValid()) {
	            $filename = "IMG-ARTICLE-".time().'.'.$file->extension();
	            $file->move(public_path().'/images_article/', $filename);
	            $url = asset("images_article/". $filename);
	        } else {
	            $message = 'An error occured while uploading the file.';
	        }
	    } else {
	        $message = 'No file uploaded.';
	    }
	    return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
	}

  public function article_detail($slug)
  {
      $data = Article::where('slug', $slug)->first();

      $user = DB::table('author_profiles')
    			->select('author_profiles.*')
    			->where('author_profiles.id_user', '=', $data->id_author)
    			->first();

      if(auth()->user()->id != $data->id_author)
      {
          return redirect()->route('author.manage.article');
      }

      return view('layouts.front.pages.blog_detail_author', compact('data', 'user'));
  }

  public function article_edit(Request $req, $slug)
  {
      $user = DB::table('author_profiles')
          ->select('author_profiles.*')
          ->where('author_profiles.id_user', '=', auth()->user()->id)
          ->first();

      $data = Article::with('tags_article')->where('slug', $slug)->first();

      if(auth()->user()->profile_author->id == $data->id_author)
      {
          return redirect()->route('author.manage.article');
      }

      if($req->method() == 'PATCH')
      {
          $rules = [
              'title' => 'required|min:3',
              'description' => 'required|min:5',
              'article' => 'required|min:15',
              'article_type' => 'required',
              'article_category' => 'required',
          ];
          $validator = Validator::make($req->all(), $rules);

          if ($validator->fails())
          {
              return redirect()->back()->withErrors($validator);
          }

          DB::beginTransaction();
          try {
              $file = null;
              if ($req->hasFile('file')) {
                  if (! $req->file('file')->isValid())
                  {
                      return redirect()->back();
                      $cover = $req->file('file');
                      $file = 'file_'.md5(microtime(true)).'.'.$cover->extension();
                      $cover->move(public_path('upload/article'), $file);
                  }
              }

              $req = $req->only('title', 'description', 'article', 'article_type', 'article_category');
              $req['file'] = $file;
              $req['id_author'] = auth()->user()->id;
              $article = $data->update($req);

              if (! $article)
              {
                  return redirect()->back();
              }

              $data->tags_article()->delete();

              foreach (Input::get('tag')  as $val)
              {
                  ArticleTag::create([
                      'id_article' => $data->id,
                      'id_tag' => $val
                  ]);
              }
          } catch (\Exception $e) {
              DB::rollback();
              throw $e;
          }
          DB::commit();

          // $req->session()->flash('success', 'Success posting an article');
          return redirect('authors/manage-article');
      }

      return view('layouts.author.pages.dashboard.edit_article', compact('user','data'));
  }

	public function manage_article(Request $req)
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$data = Article::where('id_author', auth()->user()->id)->orderBy('created_at', 'DESC')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.manage_article', compact('data', 'user', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

	public function pending_article()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$data = Article::where('id_author', auth()->user()->id)->where('status', 'pending')->get();
		return view('layouts.author.pages.dashboard.manage_article_pending', compact('data', 'user'));
	}

	public function published_article()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$data = Article::where('id_author', auth()->user()->id)->where('status', 'publish')->get();
		return view('layouts.author.pages.dashboard.manage_article_published', compact('data', 'user'));
	}

	public function blogpost_article()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$data = Article::where('id_author', auth()->user()->id)->where('status', 'blog')->get();
		return view('layouts.author.pages.dashboard.manage_article_blogpost', compact('data', 'user'));
	}

  //=====================================================================================================================

  public function article_all()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

    	$title = 'All Article';

		$data = Article::where('id_author', auth()->user()->id)->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_marketNews()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$title = 'Market News Article';
		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_marketOutlook()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

    	$title = 'Market Outlook Article';

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get();
		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_StockIdeas()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

      	$title = 'Stock Ideas Article';

		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_irPresentation()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

      	$title = 'IR Presentation Article';

		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_brokerReport()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

      	$title = 'Broker Report Article';

		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}

  public function article_funds()
	{
		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

      	$title = 'Fund Article';

		$data = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get();

		$all_count = Article::where('id_author', auth()->user()->id)->get()->count();
		$market_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$marketoutlook_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get()->count();
		$stockideas_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%3%")->orderBy('id', 'desc')->get()->count();
		$irpresentation_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%6%")->orderBy('id', 'desc')->get()->count();
		$broker_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%4%")->orderBy('id', 'desc')->get()->count();
		$funds_count = Article::where('id_author', auth()->user()->id)->where('article_type', 'LIKE', "%5%")->orderBy('id', 'desc')->get()->count();

		return view('layouts.author.pages.dashboard.articles', compact('data', 'user', 'title', 'all_count', 'market_count', 'marketoutlook_count', 'stockideas_count', 'irpresentation_count', 'broker_count', 'funds_count'));
	}



}
