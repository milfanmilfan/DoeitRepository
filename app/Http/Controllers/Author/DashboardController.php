<?php

namespace App\Http\Controllers\Author;

use App\Article;
use App\ArticleComment;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
	public function index()
	{

		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', auth()->user()->id)
			->first();

		$article = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.*')
			->where('articles.id_author', '=', auth()->user()->id)
			->get();

		return view('layouts.author.pages.dashboard.home', compact('article', 'user'));
	}

	public function login(Request $req)
	{
		if (auth()->check() && auth()->user()->author)
			return redirect('authors');

		if ($req->method() == 'POST') :

			$user = DB::table('users')
				->select('users.*')
				->where('users.email', '=', $req->get('email'))
				->first();

			$status = $user->author;
			$complete = $user->complete;

			$input = [
				'email' => $req->get('email'),
				'password' => $req->get('password'),
				'author' => $status
			];

			if($complete == 0)
			{
				$req->session()->flash('warning', 'Please check your email to complete your profile');
				return redirect()->route('author.login');
			}

			if (Auth::attempt($input))
				if (($status == 1))
				{
					return redirect('authors');
				}
				else
				{
					return redirect('account/home');
				}

			$req->session()->flash('warning', 'Wrong email / password');
	        return redirect()->back();
		endif;

		return view('layouts.author.pages.login.login');
	}

	public function blog_detail(Request $req)
	{
		$data = Article::find($req->get('id'));

		if (! $data)
		{
			return redirect('/');
		}

		if ($req->method() == 'POST') :
			$rules = [
				'comment' => 'required|min:5',
				'id_article' => 'required|integer',
			];
			$validator = Validator::make($req->all(), $rules);

			if ($validator->fails())
				return redirect()->back()->withErrors($validator);

			ArticleComment::create($req->all());
			return redirect()->back();
		endif;

		$data->load('author');

		return view('layouts.front.pages.blog_detail_author', compact('data', 'user'));
	}


}
