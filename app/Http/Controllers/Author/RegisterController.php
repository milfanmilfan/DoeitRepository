<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\Mail\VerifyAccount;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Mail;

class RegisterController extends Controller
{
	public function author_register(Request $req, $token)
	{
		if (! $token)
		{
			return redirect('/');
		}

		$check = User::where('token', $token)->first();

		if (! $check)
			return redirect('/');

		if ($req->method() == 'POST') :

			DB::beginTransaction();
			try
			{
				Validator::make($req->all(), [
					'firstname'       => 'required',
					'lastname'        => 'required',
					'phone'     => 'required|numeric',
					'datebirth'        => 'required',
					'address'          => 'required',
					'postalcode'      => 'required|numeric',
					'city'             => 'required',
					'sitename'         => 'required',
					'siteurl'          => 'required',
					'minibio'          => 'required',
					'identity'         => 'required',
					'category'         => 'required',
					'profile_pict' => 'image'
				])->validate();

				$check->name = $req->firstname.' '.$req->lastname;
				$check->token = null;
				$check->complete = 1;

				$check->save();

				$identity = $req->identity;
				$category = $req->category;

				if($req->hasFile('profile_pict'))
				{
					$imageName = time().'.'.$req->file('profile_pict')->getClientOriginalExtension();
					$req->file('profile_pict')->move(public_path('web/img/profile'), $imageName);
				}
				else{
					$imageName = 'avatar.png';
				}

				$author_profiles = DB::table('author_profiles')->insert([
					'id_user'          => $check->id,
					'first_name'       => $req->input('firstname'),
					'last_name'        => $req->input('lastname'),
					'phone_number'     => $req->input('phone'),
					'dob'              => Carbon::createFromFormat('d F Y', $req->datebirth),
					'address'          => $req->input('address'),
					'postal_code'      => $req->input('postalcode'),
					'city'             => $req->input('city'),
					'site_name'        => $req->input('sitename'),
					'site_url'         => $req->input('siteurl'),
					'bio'              => $req->input('minibio'),
					'is_firm'          => 0,
					'is_advisor'       => 0,
					'profile_pictures' => $imageName,
					'created_at'       => Carbon::now()
				]);

				foreach($category as $key => $value)
				{
					$db_category = DB::table('interest_user')->insert([
						'id_user'        => $check->id,
						'interest_value' => $value,
						'created_at'     => Carbon::now()
					]);
				}
			}
			catch (\Exception $e)
			{
				DB::rollback();
				throw $e;
				return redirect()->route('author.register', $token);
			}

			DB::commit();

			$req->session()->flash('success', 'Author account activation success');
			return redirect()->route('author.login');

		endif;

		return view('layouts.author.pages.registration.author_register', compact('token'));
	}

	public function mail_register(Request $req)
	{
		$validator = JsValidator::make([
			'email'    => 'required|email',
			'password' => 'required|min:6'
		],  [], [], '#form-register');

		if ($req->method() == 'POST')
		{
			$token = str_random(30);
	        $email = $req->get('email');

			$user = DB::table('users')
				->select('users.*')
				->where('users.email', '=', $req->get('email'))
				->first();

			if(!empty($user))
			{
				$req->session()->flash('success', 'Your Email Already Registered');
				return redirect()->route('author.mail.register');
			}
			else
			{
				DB::beginTransaction();
				try
				{

					$users = DB::table('users')->insert([
						'email'      => $req->input('email'),
						'token'      => $token,
						'password'   => bcrypt($req->password),
						'author'     => 1,
						'created_at' => Carbon::now()
					]);

					$link = 'authors/register';

					Mail::to($email)->send(new VerifyAccount($email, $token, $link));

				}
				catch (\Exception $e)
				{
					DB::rollback();
					throw $e;
					return redirect('authors/register/'.$token);
				}

				DB::commit();
				if($users)
				{
					return redirect('authors/register/welcome/'.$token);
				}
				else
				{
					return redirect('authors/register/'.$token);
				}
			}
		}

		return view('layouts.author.pages.registration.mail_register', compact('validator'));
	}

	public function welcome($token)
	{
		$user = DB::table('users')
			->select('users.*')
			->where('users.token', '=', $token)
			->first();

		if(!empty($user))
		{
			return view('layouts.author.pages.registration.greeting_register');
		}
		else
		{
			return redirect('/');
		}
	}
}
