<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
	public function logout(Request $request)
	{
		Auth::logout();
		$request->session()->flush();
		$request->session()->regenerate();
		return redirect()->guest(route( 'author.login' ));
	}
}
