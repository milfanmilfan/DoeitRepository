<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{
	public function regional()
	{
		return view('layouts.front.pages.discount.regional');
	}

	public function newest()
	{
		return view('layouts.front.pages.discount.newest');
	}

	public function likes()
	{
		return view('layouts.front.pages.discount.likes');
	}
}
