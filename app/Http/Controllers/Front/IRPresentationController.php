<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IRPresentationController extends Controller
{
	public function latest()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '6A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.ir_presentation.latest', compact('article'));
	}

	public function search_within_ir()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '6A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.ir_presentation.search_within_ir', compact('article'));
	}
}
