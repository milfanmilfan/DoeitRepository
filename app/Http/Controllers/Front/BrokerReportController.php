<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrokerReportController extends Controller
{
	public function index()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '4A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.broker_report.all', compact('article'));
	}

	public function latest()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '4A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.broker_report.latest', compact('article'));
	}

	public function search_with()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '4B')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.broker_report.search_within_broker', compact('article'));
	}
}
