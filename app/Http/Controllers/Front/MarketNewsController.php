<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketNewsController extends Controller
{
	public function index()
	{
		$top = Article::with('author')->withCount('article_comment')->where('article_type', 'LIKE', "%1%")->take(5)->orderBy('id', 'desc')->get();
		$marketnews = Article::with('author')->withCount('article_comment')->where('article_type', 'LIKE', "%1%")->orderBy('id', 'desc')->get();

		return view('layouts.front.pages.market_news.all', compact('top', 'marketnews'));
	}

	public function indonesian_economy()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '1A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.market_news.indonesian_economy', compact('article'));
	}

	public function international_economy()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '1B')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.market_news.international_economy',  compact('article'));
	}

	public function company_announcement()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '1C')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.market_news.company_announcement', compact('article'));
	}
}
