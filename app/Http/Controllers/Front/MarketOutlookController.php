<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketOutlookController extends Controller
{
	public function index()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '2A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.market_outlook.all', compact('article'));
	}
}
