<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FundsController extends Controller
{
	public function index_funds()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '5A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.funds.index_funds', compact('article'));
	}

	public function active_funds()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '5B')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.funds.active_funds', compact('article'));
	}
}
