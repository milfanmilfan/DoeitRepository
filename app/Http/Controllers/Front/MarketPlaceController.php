<?php

namespace App\Http\Controllers\Front;

use App\Mail\PaymentInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class MarketPlaceController extends Controller
{
	public function index()
	{
		if (auth()->check())
		{

			$articles = DB::table('articles')
				->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
				->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'author_profiles.profile_pictures')
				->where('articles.is_paid', '=', 1)
				->where('articles.status', '=', "publish")
				->get();

			$existing_status = DB::table('subscribe_payment')->select('subscribe_payment.*')
				->Where('subscribe_payment.id_user', auth()->user()->id)
				->Where('subscribe_payment.status', 1)
				->get()->count();

			return view('layouts.front.pages.marketplace.index', compact('articles', 'existing_status'));
		}
		else
		{
			return redirect()->route('author.login');
		}
	}

	public function information($id)
	{
		$article_id = decrypt($id);
		$articles = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->select('articles.*', 'author_profiles.first_name', 'author_profiles.last_name', 'author_profiles.profile_pictures')
			->where('articles.id', '=', $article_id)
			->first();

		return view('layouts.front.pages.marketplace.information', compact('articles'));
	}

	public function getPayment($package)
	{
		if(!empty(auth()->user()->id))
		{
//			dd($package);
			$link = 'marketplace/confirm-payment/'.str_random(30);

			$members = DB::table('users')->select('users.*')->Where('users.id', auth()->user()->id)->first();

			$must_paid = Carbon::now()->addDay();

			$existing_status = DB::table('subscribe_payment')->select('subscribe_payment.*')
				->Where('subscribe_payment.id_user', auth()->user()->id)
				->Where('subscribe_payment.status', 1)
				->first();

			if(empty($existing_status->status))
			{
				DB::beginTransaction();
				try
				{
					$subscribe_payment = DB::table('subscribe_payment')->insert([
						'id_user' 			=> auth()->user()->id,
						'order_date' 		=> Carbon::now(),
						'package' 			=> $package,
						'pay_due_date'		=> Carbon::now()->addDay(),
						'end_date'			=> Carbon::now()->addMonth(),
						'status'			=> 0,
						'created_date' 		=> Carbon::now()
					]);

				} catch (\Exception $e) {
					DB::rollback();
					throw $e;
				}
				DB::commit();

//			Mail::to($members->email)->send(new PaymentInvoice($members->name, $articles->first_name." ".$articles->last_name, $articles->title, $articles->price, $articles->description, $link, $must_paid));

				return redirect('marketplace/subscribe/wait/'.encrypt(auth()->user()->id));
			}
			else
			{
				$header = "Oops, Sorry";
				$sub_header = "Your status is active to subcribe";
				$status = 1;

				return view('layouts.front.pages.marketplace.thanks_subscribe', compact('header', 'sub_header', 'status'));
			}
		}
		else
		{
			return redirect('marketplace');
		}
	}

	public function pick_article($id)
	{
		$id_article = decrypt($id);

		if(!empty(auth()->user()->id))
		{

			$status_subscribe = DB::table('subscribe_payment')->select('subscribe_payment.*')
				->Where('subscribe_payment.id_user', auth()->user()->id)
				->Where('subscribe_payment.status', 1)
				->first();

			$existing_article = DB::table('marketplace')->select('marketplace.*')
				->Where('marketplace.id_user', auth()->user()->id)
				->Where('marketplace.id_article', $id_article)
				->get()->count();

			$picked_article = DB::table('marketplace')->select('marketplace.*')
				->Where('marketplace.id_user', auth()->user()->id)
				->Where('marketplace.id_article', $id_article)
				->get()->count();

			if($existing_article > 0)
			{
				if($status_subscribe->package = 'starter')
				{
					if($picked_article > 20 )
					{
						return $this->limit_pick_article(Session::token(), 20);
					}
				}
				elseif ($status_subscribe->package = 'intermediate')
				{
					if($picked_article > 50 )
					{
						return $this->limit_pick_article(Session::token(), 50);
					}
				}

				$article = DB::table('articles')->select('articles.*')
					->Where('articles.id', $id_article)
					->first();

				return redirect('account/paid/article/'.$article->slug);
			}

			DB::beginTransaction();
			try
			{
				$marketplace = DB::table('marketplace')->insert([
					'id_user' 			=> auth()->user()->id,
					'id_article' 		=> $id_article,
					'status'			=> 1,
					'created_date' 		=> Carbon::now()
				]);

			} catch (\Exception $e) {
				DB::rollback();
//				throw $e;/
			}
			DB::commit();

			return redirect('account/paid-articles');
		}
		else
		{
			return redirect('marketplace');
		}
	}

	public function subscribe()
	{
		if(!empty(auth()->user()->id))
		{
			return view('layouts.front.pages.marketplace.subscribe');
		}
		else
		{
			return redirect('account/sign-in');
		}
	}

	public function subscribe_message($id)
	{
		$id_user = decrypt($id);

		if(!empty(auth()->user()->id) && $id_user == auth()->user()->id)
		{
			$header = "Thanks for your subscribe";
			$sub_header = "Your invoice will be sent to your email";
			$status = 100;

			return view('layouts.front.pages.marketplace.thanks_subscribe', compact('header', 'sub_header', 'status'));
		}
		else
		{
			return redirect('account/sign-in');
		}
	}

	public function limit_pick_article($package, $amount)
	{
		if(!empty(auth()->user()->id) && ($package != Session::token()))
		{
			return redirect('account/sign-in');
		}
		else
		{
			$package_desc = decrypt($package);

			$header = "Your Subscribet amount is limit. The Package " . $package_desc . " is only " . $amount . " Report or Articles.";
			$sub_header = "Please upgrade your package";
			$status = 100;

			return view('layouts.front.pages.marketplace.thanks_subscribe', compact('header', 'sub_header', 'status'));
		}
	}


}
