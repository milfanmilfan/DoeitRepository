<?php

namespace App\Http\Controllers\Front;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockIdeasController extends Controller
{
	public function buy_ideas()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '3A')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.stockIdeas.buy_ideas', compact('article'));
	}

	public function sell_ideas()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '3B')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.stockIdeas.sell_ideas', compact('article'));
	}

	public function sectors()
	{
		$article = Article::with('author')->withCount('article_comment')->where('article_type', '3C')->orderBy('id', 'desc')->get();
		return view('layouts.front.pages.stockIdeas.sectors', compact('article'));
	}
}
