<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProController extends Controller
{
	public function index()
	{
		return view('layouts.front.pages.pro.index');
	}
}
