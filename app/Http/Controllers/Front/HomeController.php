<?php

namespace App\Http\Controllers\Front;

use App\Article;
use App\ArticleComment;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
	{

		$trending = Article::with('author')->withCount('article_comment')->get()->chunk(4);
		$latest = Article::with('author')->withCount('article_comment')->take(4)->orderBy('id', 'desc')->get();
		$top = Article::with('author')->withCount('article_comment')->take(4)->orderBy('id', 'desc')->get()->chunk(2);
		$marketnews = Article::with('author')->withCount('article_comment')->where('article_type', 'LIKE', "%2%")->take(3)->orderBy('id', 'desc')->get();
		$stockideas = Article::with('author')->withCount('article_comment')->where('article_type', 'LIKE', "%3%")->take(3)->orderBy('id', 'desc')->get();
		$indexfunds = Article::with('author')->withCount('article_comment')->where('article_type', '5A')->take(2)->orderBy('id', 'desc')->get();
		$activefunds = Article::with('author')->withCount('article_comment')->where('article_type', '5B')->take(2)->orderBy('id', 'desc')->get();

		return view('layouts.front.pages.home', compact('trending', 'latest', 'top', 'marketnews', 'stockideas', 'indexfunds', 'activefunds'));
	}

	public function blog_detail(Request $req)
	{
		$data = Article::find($req->get('id'));

		$user = DB::table('author_profiles')
			->select('author_profiles.*')
			->where('author_profiles.id_user', '=', $data->id_author)
			->first();

		if (! $data)
			return redirect('/');

		if ($req->method() == 'POST') :
			$rules = [
	            'comment' => 'required|min:5',
	            'id_article' => 'required|integer',
	        ];
	        $validator = Validator::make($req->all(), $rules);

	        if ($validator->fails())
	            return redirect()->back()->withErrors($validator);

			ArticleComment::create($req->all());
			return redirect()->back();
		endif;

		$data->load('author');

		return view('layouts.front.pages.blog_detail', compact('data', 'user'));
	}

	public function article_detail($slug, Request $req)
	{
		if(auth()->check())
		{
			$data = Article::where('slug', $slug)->first();

			$user = DB::table('author_profiles')
				->select('author_profiles.*')
				->where('author_profiles.id_user', '=', $data->id_author)
				->first();

			$status = 1;

			if(empty(auth()->user()->profile_member->id ))
			{
				$status = 0;
			}

			if (! $data)
				return redirect('/');

			if ($req->method() == 'POST') :

				$rules = [
					'comment' => 'required|min:5',
					'id_article' => 'required|integer',
				];
				$validator = Validator::make($req->all(), $rules);

				if ($validator->fails())
					return redirect()->back()->withErrors($validator);

				DB::beginTransaction();
				try
				{
					$db_category = DB::table('article_comments')->insert([
						'id_article'    => $req->input('id_article'),
						'comment' 		=> $req->input('comment'),
						'id_user' 		=> auth()->user()->id,
						'email' 		=> $req->input('email'),
						'name' 			=> $req->input('name'),
						'created_at'    => Carbon::now()
					]);
				}
				catch(\Exception $e)
				{
					DB::rollback();
					return redirect()->back();
				}
				DB::commit();

				$req->session()->flash('status', 'Your Comment will be approved by Author');
				return redirect()->back();
			endif;

			return view('layouts.front.pages.blog_detail', compact('data', 'user', 'status'));
		}
		else
		{
			return redirect('account/sign-in');
		}
	}
}
