<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ArticleRequestController extends Controller
{
    public function index()
	{
		$articles = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('articles.id', 'articles.title', 'articles.article_type', 'author_profiles.profile_pictures', 'users.name', 'users.email')
			->where('articles.status', '=', 'pending')
			->get();

		return view('layouts.admin.pages.article_request', compact('articles'));
	}

	public function edit($id)
	{
		$article = Article::with('author')->where('id', $id)->where('status', 'pending')->first();

		return view('layouts.admin.pages.article_request_view', compact('article'));
	}

	public function update(Request $req, $id)
	{
		$article = Article::with('author')->where('id', $id)->where('status', 'pending')->first();

		if(empty($article))
		{
			Session::flash('message-error', 'Article Not Found');
			return redirect()->route('admin.article_request');
		}

		$article->title        = $req->title;
		$article->article_type = $req->article_type;
		$article->description  = $req->summary;
		$article->article      = $req->article_content;

		if($req->btn_save == 'publish') {
			$article->status = 'publish';
			$message         = 'Published';
		}
		elseif($req->btn_save == 'reject') {
			$article->status = 'reject';
			$message         = 'Rejected';
		}
		else {
			Session::flash('message-error', 'Something Wrong!');
			return redirect()->route('admin.article_request');
		}

		$article->save();

		Session::flash('message', 'Article '.$message);
		return redirect()->route('admin.article_request');
	}
}
