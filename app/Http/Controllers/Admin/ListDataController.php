<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ListDataController extends Controller
{
    public function member()
	{
		$data = DB::table('member_profiles')
			->join('users', 'member_profiles.id_user', '=', 'users.id')
			->select('member_profiles.*', 'users.name', 'users.email')
			->where('users.author', '=', 0)
			->get();

		return view('layouts.admin.pages.list_data.member', compact('data'));
	}

	public function intern_author()
	{
		$data = DB::table('author_profiles')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('author_profiles.*', 'users.name', 'users.email')
			->where('users.author', '=', 1)
			->get();

		return view('layouts.admin.pages.list_data.intern_author', compact('data'));
	}

	public function extern_author()
	{
		$data = DB::table('author_profiles')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('author_profiles.*', 'users.name', 'users.email')
			->where('users.author', '=', 2)
			->get();

		return view('layouts.admin.pages.list_data.extern_author', compact('data'));
	}
}
