<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
	{
		return view('layouts.admin.pages.dashboard');
	}

	public function login()
	{
		return view('auth.admin.login');
	}
}
