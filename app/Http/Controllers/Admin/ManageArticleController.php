<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ManageArticleController extends Controller
{
    public function published()
	{
		$articles = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('articles.id', 'articles.title', 'articles.article_type', 'author_profiles.profile_pictures', 'users.name', 'users.email')
			->where('articles.status', '=', 'publish')
			->get();

		$title = "Published Articles";
		$remarks = "List of Published Article";

		return view('layouts.admin.pages.manage_article.manage_article', compact('articles', 'title', 'remarks'));
	}

	public function rejected()
	{
		$articles = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('articles.id', 'articles.title', 'articles.article_type', 'author_profiles.profile_pictures', 'users.name', 'users.email')
			->where('articles.status', '=', 'reject')
			->get();

		$title = "Rejected Articles";
		$remarks = "List of Rejected Article";

		return view('layouts.admin.pages.manage_article.manage_article', compact('articles', 'title', 'remarks'));
	}

	public function suspend()
	{
		$articles = DB::table('articles')
			->join('author_profiles', 'author_profiles.id_user', '=', 'articles.id_author')
			->join('users', 'author_profiles.id_user', '=', 'users.id')
			->select('articles.id', 'articles.title', 'articles.article_type', 'author_profiles.profile_pictures', 'users.name', 'users.email')
			->where('articles.status', '=', 'suspend')
			->get();

		$title = "Suspended Articles";
		$remarks = "List of Suspended Article";

		return view('layouts.admin.pages.manage_article.manage_article', compact('articles', 'title', 'remarks'));
	}
}
