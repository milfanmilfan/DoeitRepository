<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class InternAuthorController extends Controller
{
	protected $rules;

	public function __construct()
	{
		$this->rules = [
			'first_name'   => 'required',
			'last_name'    => 'required',
			'dob'          => 'required',
			'address'      => 'required',
			'city'         => 'required',
			'postal_code'  => 'required',
			'phone_number' => 'required'
		];
	}

    public function add()
	{
		return view('layouts.admin.pages.intern_author.add');
	}

	public function store(Request $req)
	{
		DB::beginTransaction();
		try
		{
			Validator::make($req->all(), [
				'email'           => 'required|email',
				'password'        => 'required|min:6|confirmed',
				'first_name'      => 'required',
				'last_name'       => 'required',
				'phone_number'    => 'required|numeric',
				'dob'             => 'required',
				'address'         => 'required',
				'postal_code'     => 'required|numeric',
				'city'            => 'required',
				'bio'             => 'required',
				'category'        => 'required',
				'profile_picture' => 'nullable|image'
			])->validate();

			$user = User::create([
				'name'     => $req->first_name.' '.$req->last_name,
				'email'    => $req->email,
				'password' => bcrypt($req->password),
				'author'   => 1,
				'complete' => 1
			]);

			$category = $req->category;

			if($req->hasFile('profile_picture'))
			{
				$imageName = time().'.'.$req->file('profile_picture')->getClientOriginalExtension();
				$req->file('profile_picture')->move(public_path('web/img/profile'), $imageName);
			}
			else{
				$imageName = 'avatar.png';
			}

			$author_profiles = DB::table('author_profiles')->insert([
				'id_user'          => $user->id,
				'first_name'       => $req->input('first_name'),
				'last_name'        => $req->input('last_name'),
				'phone_number'     => $req->input('phone_number'),
				'dob'              => Carbon::createFromFormat('d/m/y', $req->dob),
				'address'          => $req->input('address'),
				'postal_code'      => $req->input('postal_code'),
				'city'             => $req->input('city'),
				'site_name'        => $req->input('site_name'),
				'site_url'         => $req->input('site_url'),
				'bio'              => $req->input('minibio'),
				'is_firm'          => 0,
				'is_advisor'       => 1,
				'profile_pictures' => $imageName,
				'created_at'       => Carbon::now()
			]);

			foreach($category as $key => $value)
			{
				$db_category = DB::table('interest_user')->insert([
					'id_user'        => $user->id,
					'interest_value' => $value,
					'created_at'     => Carbon::now()
				]);
			}
		}
		catch (\Exception $e)
		{
			DB::rollback();
			throw $e;
			return redirect()->route('admin.intern_author_add');
		}

		DB::commit();

		Session::flash('message', 'Intern Author has Inserted');
		return redirect()->route('admin.intern_author');
	}
}
