<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentInvoice extends Mailable
{
    use Queueable, SerializesModels;

	public $member_name;
	public $author;
	public $title;
	public $price;
	public $article_content;
	public $link;
	public $must_paid;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($member_name, $author, $title, $price, $article_content, $link, $must_paid)
    {
		$this->member_name = $member_name;
		$this->author = $author;
		$this->title = $title;
		$this->price = $price;
		$this->article_content = $article_content;
		$this->link = $link;
		$this->must_paid = $must_paid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.paymentInvoice');
    }
}
