<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use Sluggable;

    protected $fillable = ['title', 'slug', 'description', 'article', 'article_type', 'article_category', 'file', 'id_author'];

    public function author()
    {
    	return $this->belongsTo('App\User', 'id_author');
    }

    public function tags_article()
    {
        return $this->hasMany('App\ArticleTag', 'id_article');
    }

    public function article_comment()
    {
    	return $this->hasMany('App\ArticleComment', 'id_article');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
