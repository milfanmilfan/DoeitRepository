<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberBroker extends Model
{
	protected $guarded = ['id'];
}
