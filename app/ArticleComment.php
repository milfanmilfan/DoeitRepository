<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleComment extends Model
{
    protected $fillable = ['id_article', 'comment', 'id_user', 'email', 'name'];
}
