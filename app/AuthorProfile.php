<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorProfile extends Model
{
	protected $guarded = ['id'];
}
