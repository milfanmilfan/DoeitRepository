<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTicker extends Model
{
    protected $fillable = ['id_article', 'id_ticker'];
}
