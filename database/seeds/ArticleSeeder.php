<?php

use App\Article;
use App\ArticleTag;
use App\ArticleTicker;
use App\ArticleComment;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Article::truncate();
        // ArticleComment::truncate();
        // ArticleTicker::truncate();
        // ArticleTag::truncate();

        factory(Article::class, 50)->create()->each(function($art) {

            $art->id_author = 22;
            $art->save();

            factory(ArticleComment::class, mt_rand(5, 10))->create(['id_article' => $art->id]);

        });
    }
}
