<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('author_profiles', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('id_user')->unsigned()->default(0);

           $table->string('first_name')->nullable();
           $table->string('last_name')->nullable();
           $table->string('phone_number')->nullable();

           $table->date('dob')->nullable();
           $table->string('address')->nullable();
           $table->string('postal_code')->nullable();
           $table->string('city')->nullable();

           $table->string('site_name')->nullable();
           $table->string('site_url')->nullable();
           $table->string('bio')->nullable();

           $table->boolean('is_firm')->default(false);
           $table->boolean('is_advisor')->default(false);
			     $table->string('profile_pictures')->nullable();
           $table->string('facebook')->nullable();
           $table->string('twitter')->nullable();
           $table->string('skype')->nullable();
           $table->string('google')->nullable();
           $table->string('linkedin')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('author_profiles');
    }
}
