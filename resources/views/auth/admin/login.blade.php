<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Doeit Web Administrator</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
    <link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('admin_asset/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_asset/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_asset/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_asset/css/themes.css') }}">
    <script src="{{ asset('admin_asset/js/vendor/modernizr.min.js') }}"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <div id="login-alt-container">
                <img src="{{ asset('web/img/logo2.png') }}" alt="" class="login_logo">
                <h1 class="push-top-bottom">
                    <br>
                    <small>Welcome to Doeit Administrator</small>
                </h1>
                <ul class="fa-ul text-muted">
                    <li><i class=""></i> &nbsp;</li>
                    <li><i class=""></i> &nbsp;</li>
                    <li><i class=""></i> &nbsp;</li>
                    <li><i class=""></i> &nbsp;</li>
                    <li><i class=""></i> &nbsp;</li>
                </ul>
            </div>
        </div>
        <div class="col-md-5">
            <div id="login-container">
                <div class="login-title text-center">
                    <h1><strong>Administrator Login</strong></h1>
                </div>
                <div class="block push-bit">
                    <form action="" method="post" id="form-login" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                    <input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                    <input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-xs-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                                    <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-xs-8 text-right">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a>
                            </div>
                        </div>
                    </form>
                </div>
                <footer class="text-muted push-top-bottom pull-right">
                    <small>2017 &copy; <a href="http://doeit.org/" target="_blank">Doeit.org</a></small>
                </footer>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
<script src="{{ asset('admin_asset/js/app.js') }}"></script>
<script src="{{ asset('admin_asset/js/pages/login.js') }}"></script>
<script>$(function(){ Login.init(); });</script>
</body>
</html>