<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div dir="ltr">
    <table bgcolor="#f1f1f1" border="0" cellpadding="0" cellspacing="0" height="100%" width="700px" style="color:rgb(0,0,0);font-family:&quot;times new roman&quot;;font-size:medium;background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(241,241,241);border-collapse:collapse;margin:0px;padding:0px;height:671px;width:700px">
        <tbody>
        <tr>
            <td align="center" valign="top" style="border-top-width:0px;padding:20px 10px;height:631px;width:1313px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border:0px;max-width:600px">
                    <tbody>
                    <tr>
                        <td align="center" bgcolor="#ffffff" valign="top" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(255,255,255);border-bottom-width:0px;border-top-width:0px">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                <tbody>
                                <tr>
                                    <td valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                            <tbody>
                                            <tr>
                                                <td style="min-width:100%;padding:2px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%">
                                                        <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                <tbody>
                                <tr>
                                    <td valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                            <tbody>
                                            <tr style="font-family:Segoe UI;font-size:15px">
                                                <td align="left" valign="top" style="color:rgb(102,102,102);line-height:20px;padding:9px 18px">
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="color:rgb(102,102,102); line-height:20px;border-collapse:collapse;min-width:100%; margin-bottom: 20px;">
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <img src="http://doeit.org/img/logo_only.png" alt="" style="width: 50%;">
                                                            </td>
                                                            <td style="width: 50%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p>
                                                        <span style="font-family:Segoe UI">Hello, {{ $member_name }}</span>
                                                    </p>
                                                    <p>
                                                        <span style="font-family:Segoe UI">
                                                            Thank you for subscribe our Article at <a style="color:#2a8fbd; text-decoration: none" href="{{env('APP_URL')}}" target="_blank"><strong>Doeit.org</strong></a>. <br>
                                                            Your order will be sent soon.
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span style="font-family:Segoe UI">
                                                            <strong>Article Detail</strong>
                                                            <br>
                                                            <strong>Author :</strong>
                                                            <br>
                                                            {{ $author }}
                                                            <br>
                                                            <strong>Title :</strong>
                                                            <br>
                                                            {{ $title }}
                                                            <br>
                                                            <strong>Article :</strong>
                                                            {!! $article_content !!}
                                                        </span>
                                                    </p>
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="color:rgb(102,102,102); line-height:20px;border-collapse:collapse;min-width:100%; margin-bottom: 20px;">
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <strong>Subscribe Amount</strong>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <strong>Must be paid until</strong>
                                                            </td>
                                                        </tr>
                                                        <td>
                                                            Rp. {{ number_format($price) }}
                                                        </td>
                                                        <td>
                                                            {{ $must_paid }}
                                                        </td>
                                                    </table>
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="color:rgb(102,102,102); line-height:20px;border-collapse:collapse;min-width:100%; margin-bottom: 20px;">
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <strong>Payment Method</strong>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <strong>Payment Status</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <img src="http://beta.abidenim.com/back_assets/img/content/email/bca.png" alt="" style="width: 30%;"><br>
                                                                Transfer BCA <br>
                                                                xxx xxx xxxx <br>
                                                                a/n Milfan Pahlawan <br>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                Waiting for Payment
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p>
                                                        <span style="font-family:Segoe UI; color:rgb(102,102,102);line-height:20px;">To complete the order process, please confirm payment by clicking the confirmation button below,</span>
                                                    </p>
                                                    <p align="center">
                                                    </p>
                                                    <table bgcolor="#C1181E" border="0" cellpadding="0" cellspacing="0" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:#2a8fbd;border-radius:3px">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="middle" style="font-size:16px;font-family:Segoe UI;padding:15px"> <a href="{{env('APP_URL')}}/{{$link}}" title="-" style="color:rgb(255,255,255);display:block;line-height:16px;text-decoration:none;word-wrap:break-word" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=id&q=http://192.168.43.112/efarm/front.html&source=gmail&ust=1483526279444000&usg=AFQjCNFxVF1dz3Adz5XnDR0WVax5g_-Ifg">Confirm Your Payment</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <p>
                                                        <span style="font-family:Segoe UI;color:rgb(102,102,102);line-height:20px;">For the sake of the security of your transactions, be sure not to inform proof and payment data to any party except <strong>Doeit.org</strong></span>
                                                    </p>
                                                    <hr>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 70%;">
                                                                <p style="font-size: 11px; text-align: left; font-family:Segoe UI;">
                                                                    <strong>Doeit.org</strong> <br>
                                                                    Lorem Stree <br>
                                                                    Ciledug <br>
                                                                    Tangerang, Banten <br>
                                                                    Indonesia <br>
                                                                    <br>
                                                                </p>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <p style="font-size: 11px; text-align: left;font-family:Segoe UI;">
                                                                    Our mailing address is:
                                                                    <br>
                                                                    contact@doeit.org <br>
                                                                    <br>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                    <span style="font-family:Segoe UI;font-size: 11px;">This emails are generated automatically. Please do not send a reply to this email.</span>
                                                    <br>
                                                    <span style="font-family:Segoe UI;font-size: 11px;">Copyright © 2017 Doeit.org, All rights reserved.</span>
                                                    <p>
                                                        &nbsp;
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>