<div dir="ltr">
    <table bgcolor="#f1f1f1" border="0" cellpadding="0" cellspacing="0" height="100%" width="700px" style="color:rgb(0,0,0);font-family:&quot;times new roman&quot;;font-size:medium;background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(241,241,241);border-collapse:collapse;margin:0px;padding:0px;height:671px;width:700px">
        <tbody>
            <tr>
                <td align="center" valign="top" style="border-top-width:0px;padding:20px 10px;height:631px;width:1313px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border:0px;max-width:600px">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#f1f1f1" valign="top" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;border-bottom:15px solid rgb(179,26,32);border-top-width:0px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody>
                                            <tr>
                                                <td valign="top" style="margin-bottom:10px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center" valign="top" style="color:rgb(255,255,255);font-family:helvetica;font-size:11px;line-height:16.94px;padding:9px 18px;text-align:center">
                                                                                    <img src="layout/image/logo2.png" class="m_-5669403571674642294gmail-CToWUd CToWUd">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#ffffff" valign="top" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(255,255,255);border-bottom-width:0px;border-top-width:0px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" valign="center" width="100%" style="border-collapse:collapse;min-width:100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="color:rgb(102,102,102);font-size:15px;line-height:23.1px;padding:18px;background-color:rgb(193,24,30)">
                                                                                    <div align="center" style="text-align:center"><span style="font-family:arial,&quot;helvetica neue&quot;,helvetica,sans-serif"><span style="color:rgb(46,200,102)"><span style="font-size:20px;color:rgb(255,255,255)">Doeit</span></span></span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="min-width:100%;padding:2px">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;min-width:100%">
                                                                        <tbody>
                                                                            <tr style="font-family:helvetica,arial,sans-serif;font-size:15px">
                                                                                <td align="left" valign="top" style="color:rgb(102,102,102);line-height:23.1px;padding:9px 18px">
                                                                                    <p><span style="font-family:helvetica,arial,sans-serif">Terima kasih telah melakukan pendaftaran di <a style="color:rgb(220,85,85)" href="{{env('APP_URL')}}" target="_blank">Doeit.org</a>. <br> Untuk memulai proses registrasi, mohon untuk melakukan verifikasi email dengan mengakses link di bawah:</span>
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" style="padding:18px">
                                                                    <table bgcolor="#C1181E" border="0" cellpadding="0" cellspacing="0" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(193,24,30);border-radius:3px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center" valign="middle" style="font-size:16px;font-family:helvetica,arial,sans-serif;padding:15px">
                                                                                    <a href="{{env('APP_URL')}}/{{$link}}/{{$token}}" title="-" style="color:rgb(255,255,255);display:block;line-height:16px;text-decoration:none;word-wrap:break-word" target="_blank" data-saferedirecturl="">Link Aktifasi</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#FFFFFF" valign="top" style="background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-origin:initial;background-clip:initial;background-color:rgb(255,255,255);border-bottom-width:0px;border-top-width:0px">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" style="display:inline-block;text-align:center">
                                                    <div style="display:inline-block;vertical-align:top;width:197px">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:197px">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="display:inline-block;vertical-align:top;width:197px">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:197px">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="display:inline-block;vertical-align:top;width:197px">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:197px">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>