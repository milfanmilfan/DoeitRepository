@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="fa fa-table"></i>Member<br><small>List of Member</small>
                </h1>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center"><i class="gi gi-user"></i></th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>BirtDate</th>
                            <th>Site Name</th>
                            <th>Site URL</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data AS $row=>$member)
                        <tr>
                            <td class="text-center">{{ ++$row }}</td>
                            <td class="text-center">
                                @if(isset($member->profile_pictures))
                                <img src="{{ asset('web/img/profile') }}/{{ $member->profile_pictures }}" alt="avatar" class="img-circle" style="width: 50px; height: 50px;">
                                @else
                                    <img src="{{ asset('web/img/profile/avatar.png') }}" alt="avatar" class="img-circle" style="width: 50px; height: 50px;">
                                @endif
                            </td>
                            <td><a href="javascript:void(0)">{{ $member->name }}</a></td>
                            <td>{{ $member->email }}</td>
                            <td>
                                @if(isset($member->dob))
                                    {{ $member->dob }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td>
                                @if(isset($member->site_name))
                                    {{ $member->site_name }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td>
                                @if(isset($member->site_url))
                                    {{ $member->site_url }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('admin/article-request/edit/'.$member->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/tablesDatatables.js') }}"></script>

    <script>$(function(){ Index2.init(); TablesDatatables.init(); });</script>
@endsection