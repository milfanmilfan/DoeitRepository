@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="fa fa-table"></i>Author Request<br><small>List of Author Request</small>
                </h1>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center"><i class="gi gi-user"></i></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>BirtDate</th>
                        <th>Site Name</th>
                        <th>Site URL</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/tablesDatatables.js') }}"></script>

    <script>$(function(){ Index2.init(); TablesDatatables.init(); });</script>
@endsection