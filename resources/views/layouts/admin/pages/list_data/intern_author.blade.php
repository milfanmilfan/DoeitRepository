@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="fa fa-table"></i>Intern Author<br><small>List of Intern Author</small>
                </h1>
                <a href="{{ url('admin/intern-author/add') }}" class="btn btn-primary">New Intern Author</a>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center"><i class="gi gi-user"></i></th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>BirtDate</th>
                            <th>Site Name</th>
                            <th>Site URL</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data AS $row=>$author)
                        <tr>
                            <td class="text-center">{{ ++$row }}</td>
                            <td class="text-center">
                                @if(isset($author->profile_pictures))
                                    <img src="{{ asset('web/img/profile') }}/{{ $author->profile_pictures }}" alt="avatar" class="img-circle" style="width: 50px; height: 50px;">
                                @else
                                    <img src="{{ asset('web/img/profile/avatar.png') }}" alt="avatar" class="img-circle" style="width: 50px; height: 50px;">
                                @endif
                            </td>
                            <td><a href="javascript:void(0)">{{ $author->name }}</a></td>
                            <td>{{ $author->email }}</td>
                            <td>
                                @if(isset($author->dob))
                                    {{ Carbon\Carbon::createFromFormat('Y-m-d', $author->dob)->format('d F Y') }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td>
                                @if(isset($author->site_name))
                                    {{ $author->site_name }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td>
                                @if(isset($author->site_url))
                                    {{ $author->site_url }}
                                @else
                                    Not Set
                                @endif
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('admin/article-request/edit/'.$author->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/tablesDatatables.js') }}"></script>

    <script>$(function(){ Index2.init(); TablesDatatables.init(); });</script>
@endsection