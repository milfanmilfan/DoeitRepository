@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header content-header-media">
            <div class="header-section">
                <div class="row">
                    <div class="col-md-4 col-lg-8 hidden-xs hidden-sm">
                        <h1>Welcome <strong>Admin</strong><br><small>You Look Awesome!</small></h1>
                    </div>
                    <div class="col-md-8 col-lg-4">
                        <div class="row text-center">
                            <div class="col-xs-3 col-sm-3">
                                <h2 class="animation-hatch">
                                    $<strong>93.7k</strong><br>
                                    <small><i class="fa fa-user"></i> Member</small>
                                </h2>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <h2 class="animation-hatch">
                                    <strong>167k</strong><br>
                                    <small><i class="fa fa-user"></i> Author</small>
                                </h2>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <h2 class="animation-hatch">
                                    <strong>101</strong><br>
                                    <small><i class="fa fa-book"></i> Articles</small>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src="{{ asset('admin_asset/img/placeholders/headers/dashboard_header.jpg') }}" alt="header image" class="animation-pulseSlow">
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key="></script>
    <script src="{{ asset('admin_asset/js/helpers/gmaps.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/index2.js') }}"></script>
    <script>$(function(){ Index2.init(); });</script>
@endsection