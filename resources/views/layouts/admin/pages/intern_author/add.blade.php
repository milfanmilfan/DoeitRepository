@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="gi gi-user"></i>New Intern Author<br><small>Application for Intern Author</small>
                </h1>
            </div>
        </div>
        @include('layouts.admin.core.alert')
        @include('layouts.admin.core.validate')
        <div class="block">
            <div class="block-title">
                <h2>&nbsp;</h2>
            </div>
            <form id="intern-wizard" action="{{ route('admin.intern_author.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {!! csrf_field() !!}
                <div id="intern-first" class="step">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <ul class="nav nav-pills nav-justified intern-steps">
                                <li class="active"><a href="javascript:void(0)" data-gotostep="intern-first"><strong>1. Personal Detial</strong></a></li>
                                <li><a href="javascript:void(0)" data-gotostep="intern-second"><strong>2. Article Profile</strong></a></li>
                                <li><a href="javascript:void(0)" data-gotostep="intern-third"><strong>3. Author Pages</strong></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" id="email" value="{{ old('email') }}" name="email" class="form-control" placeholder="Email">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password <span class="text-danger">*</span></label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                </div>

                                <div class="form-group">
                                    <label for="password_confirmation">Password Confirmation <span class="text-danger">*</span></label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Password Confirmation">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input type="text" id="first_name" value="{{ old('first_name') }}" name="first_name" class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                        <input type="text" id="last_name"  value="{{ old('last_name') }}" name="last_name" class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="dob">Date of Birth <span class="text-danger">*</span></label>
                                        <input type="text" id="dob"  value="{{ old('dob') }}" name="dob" class="form-control input-datepicker-close" data-date-format="dd MM yy" placeholder="Date of Birth">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="address">Address <span class="text-danger">*</span></label>
                                        <textarea id="address" name="address" rows="5" class="form-control" placeholder="Address">{{ old('address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="city">City <span class="text-danger">*</span></label>
                                        <input type="text" id="city"  value="{{ old('city') }}" name="city" class="form-control" placeholder="City">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="postal_code">Postal Code <span class="text-danger">*</span></label>
                                        <input type="number" id="postal_code"  value="{{ old('postal_code') }}" name="postal_code" class="form-control" placeholder="Postal Code">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number <span class="text-danger">*</span></label>
                                        <input type="number" id="phone_number"  value="{{ old('phone_number') }}" name="phone_number" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="intern-second" class="step">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <ul class="nav nav-pills nav-justified intern-steps">
                                <li><a href="javascript:void(0)" data-gotostep="intern-first"><i class="fa fa-check"></i> <strong>1. Personal Detial</strong></a></li>
                                <li class="active"><a href="javascript:void(0)" data-gotostep="intern-second"><strong>2. Article Profile</strong></a></li>
                                <li><a href="javascript:void(0)" data-gotostep="intern-third"><strong>3. Author Pages</strong></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">&nbsp;</div>
                        <div class="col-sm-1">
                            &nbsp;
                        </div>
                        <div class="col-sm-4">
                            <img src="{{ asset('admin_asset/img/placeholders/avatars/avatar6.jpg') }}" accept="image/*" alt="avatar" class="widget-image img-circle" width="200px;">
                            <div class="form-group">
                                <p>&nbsp;</p>
                                <input type="file" id="profile_picture" name="profile_picture">
                                <p>
                                    Upload a headshot of yourself or an invesment related images.
                                    If youare using a pen name, please don't use a headshot.
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <fieldset>
                                <legend><i class="fa fa-angle-right"></i>  SITENAME & URL</legend>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="site_name">Site Name</label>
                                        <input type="text" id="site_name"  value="{{ old('site_name') }}" name="site_name" class="form-control" placeholder="Site Name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="site_url">Site URL</label>
                                        <input type="text" id="site_url"  value="{{ old('site_url') }}" name="site_url" class="form-control" placeholder="Site URL">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend><i class="fa fa-angle-right"></i>  Mini Bio</legend>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="bio">Mini Biography <span class="text-danger">*</span></label>
                                        <textarea id="bio" name="bio" rows="9" class="form-control" placeholder="Mini Biography">{{ old('bio') }}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-2">&nbsp;</div>
                    </div>
                </div>
                <div id="intern-third" class="step">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <ul class="nav nav-pills nav-justified intern-steps">
                                <li><a href="javascript:void(0)" data-gotostep="intern-first"><i class="fa fa-check"></i> <strong>1. Personal Detial</strong></a></li>
                                <li><a href="javascript:void(0)" data-gotostep="intern-second"><i class="fa fa-check"></i> <strong>2. Article Profile</strong></a></li>
                                <li class="active"><a href="javascript:void(0)" data-gotostep="intern-third"><strong>3. Author Pages</strong></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  FINANCIAL ADVISOR</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1A" name="category[]" @if(is_array(old('category')) && in_array("1A", old('category'))) checked @endif> Registered Investing Advisor (RIA)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2A" name="category[]" @if(is_array(old('category')) && in_array("2A", old('category'))) checked @endif> Certified Financial Planner (CFP)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3A" name="category[]" @if(is_array(old('category')) && in_array("3A", old('category'))) checked @endif> Licensed Securities Brokers
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="4A" name="category[]" @if(is_array(old('category')) && in_array("4A", old('category'))) checked @endif> Insurance Brokers
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="5A" name="category[]" @if(is_array(old('category')) && in_array("5A", old('category'))) checked @endif> Accountant
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  HEDGE FUNDS</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1B" name="category[]" @if(is_array(old('category')) && in_array("1B", old('category'))) checked @endif> Hedge Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2B" name="category[]" @if(is_array(old('category')) && in_array("2B", old('category'))) checked @endif> Hedge Fund Analyst
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3B" name="category[]" @if(is_array(old('category')) && in_array("3B", old('category'))) checked @endif> Hedge Fund Trader
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="4B" name="category[]" @if(is_array(old('category')) && in_array("4B", old('category'))) checked @endif> Hedge Fund Director of Research
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="5B" name="category[]" @if(is_array(old('category')) && in_array("5B", old('category'))) checked @endif> Hedge Fund Executive (CFO/COO/CEO)
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  MUTUAL FUNDS</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1C" name="category[]" @if(is_array(old('category')) && in_array("1C", old('category'))) checked @endif> Mutual Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2C" name="category[]" @if(is_array(old('category')) && in_array("2C", old('category'))) checked @endif> Mutual Fund Analyst
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3C" name="category[]" @if(is_array(old('category')) && in_array("3C", old('category'))) checked @endif> Mutual Fund Trader
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="4C" name="category[]" @if(is_array(old('category')) && in_array("4C", old('category'))) checked @endif> Mutual Fund Sales
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="5C" name="category[]" @if(is_array(old('category')) && in_array("5C", old('category'))) checked @endif> Hedge Fund Executive (CFO/COO/CEO)
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-4">
                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  PENSION FUNDS</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1D" name="category[]" @if(is_array(old('category')) && in_array("1D", old('category'))) checked @endif> Endowment Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2D" name="category[]" @if(is_array(old('category')) && in_array("2D", old('category'))) checked @endif> Endowment Fund Analyst
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  ENDOWMENT FUNDS</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1E" name="category[]" @if(is_array(old('category')) && in_array("1E", old('category'))) checked @endif> Endowment Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2E" name="category[]" @if(is_array(old('category')) && in_array("2E", old('category'))) checked @endif> Endowment Fund Analyst
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  PRIVE EQUITY</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1F" name="category[]" @if(is_array(old('category')) && in_array("1F", old('category'))) checked @endif> Prive Equity Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2F" name="category[]" @if(is_array(old('category')) && in_array("2F", old('category'))) checked @endif> Prive Equity Fund Analyst
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  VENTURE CAPITAL</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1G" name="category[]" @if(is_array(old('category')) && in_array("1G", old('category'))) checked @endif> Venture Capital Fund Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2G" name="category[]" @if(is_array(old('category')) && in_array("2G", old('category'))) checked @endif> Venture Capital Fund Analyst
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  COMPANY EXECUTIVE</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1H" name="category[]" @if(is_array(old('category')) && in_array("1H", old('category'))) checked @endif> CEO
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2H" name="category[]" @if(is_array(old('category')) && in_array("2H", old('category'))) checked @endif> CFO
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3H" name="category[]" @if(is_array(old('category')) && in_array("3H", old('category'))) checked @endif> COO
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="4H" name="category[]" @if(is_array(old('category')) && in_array("4H", old('category'))) checked @endif> Board Member
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="5H" name="category[]" @if(is_array(old('category')) && in_array("5H", old('category'))) checked @endif> Small Bussiness Owner
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="6H" name="category[]" @if(is_array(old('category')) && in_array("6H", old('category'))) checked @endif> Other C-Level Excetive
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-4">
                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  OTHER FINANCIALS</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1I" name="category[]" @if(is_array(old('category')) && in_array("1I", old('category'))) checked @endif> Investment Consultan
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2I" name="category[]" @if(is_array(old('category')) && in_array("2I", old('category'))) checked @endif> Investment Banker/Analyst
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3I" name="category[]" @if(is_array(old('category')) && in_array("3I", old('category'))) checked @endif> Family Office Manager/Analyst
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="4I" name="category[]" @if(is_array(old('category')) && in_array("4I", old('category'))) checked @endif> Sell Side Research Analyst/Trader
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="5I" name="category[]" @if(is_array(old('category')) && in_array("5I", old('category'))) checked @endif> Commecial Banker
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="6I" name="category[]" @if(is_array(old('category')) && in_array("6I", old('category'))) checked @endif> Operation Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="7I" name="category[]" @if(is_array(old('category')) && in_array("7I", old('category'))) checked @endif> Complience Manager
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="8I" name="category[]" @if(is_array(old('category')) && in_array("8I", old('category'))) checked @endif> IR Professional
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset style="margin-left: 20px;">
                                <legend><i class="fa fa-angle-right"></i>  INDIVIDUAL INVESTOR</legend>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1J" name="category[]" @if(is_array(old('category')) && in_array("1J", old('category'))) checked @endif> Full-time Investor
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="2J" name="category[]" @if(is_array(old('category')) && in_array("2J", old('category'))) checked @endif> Occasional Investor
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="3J" name="category[]" @if(is_array(old('category')) && in_array("3J", old('category'))) checked @endif> Retiree
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-12 col-md-offset-10">
                        <button type="reset" class="btn btn-sm btn-warning" id="back4">Previous</button>
                        <button type="submit" class="btn btn-sm btn-primary" id="next4">Next</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/formsWizard.js') }}"></script>
    <script type="text/javascript">
        /* Initialize Advanced Wizard with Validation */
        $('#intern-wizard').formwizard({
            disableUIStyles: true,
            validationEnabled: true,
            validationOptions: {
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'span',
                errorPlacement: function(error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#password'
                    },
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    dob: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    phone_number: {
                        required: true,
                        number: true
                    },
                    postal_code: {
                        required: true,
                        number: true
                    },
                    site_url: {
                        url: true
                    },
                    bio: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: 'Please Enter a First Name',
                        email: 'Email not Valid'
                    },
                    password: {
                        required: 'Please Enter a Password'
                    },
                    password_confirmation: {
                        required: 'Please Enter a Password Confirmation',
                        email: 'Password Confirmation not Match'
                    },
                    first_name: 'Please Enter a First Name',
                    last_name: 'Please Enter a Last Name',
                    dob: 'Please Enter a Date of Birth',
                    address: 'Please Enter a Address',
                    city: 'Please Enter a City',
                    phone_number: {
                        required: 'Please Enter a Phone Number',
                        number: 'Phone Number Only Accept Numeric'
                    },
                    postal_code: {
                        required: 'Please Enter a Postal Code',
                        number: 'Postal Code Only Accept Numeric'
                    },
                    site_url: 'Site URL not Valid',
                    bio: 'Please Enter a Mini Biography'
                }
            },
            inDuration: 0,
            outDuration: 0
        });

        $(document).ready(function(){
            var file = document.getElementById('profile_picture');
            $('#profile_picture').change(function(){
                showPreview(file);
            });
            profile_picture
        });
    </script>
    <script>$(function(){ FormsWizard.init(); });</script>
@endsection