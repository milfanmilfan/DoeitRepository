@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="fa fa-table"></i>{{ $title }}<br><small>{{ $remarks }}</small>
                </h1>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center"><i class="gi gi-user"></i></th>
                        <th>Client</th>
                        <th>Email</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles AS $row=>$article)
                        <tr>
                            <td class="text-center">{{ ++$row }}</td>
                            <td class="text-center"><img src="{{ asset('web/img/profile') }}/{{ $article->profile_pictures }}" alt="avatar" class="img-circle" style="width: 50px; height: 50px;"></td>
                            <td><a href="javascript:void(0)">{{ $article->name }}</a></td>
                            <td>{{ $article->email }}</td>
                            <td>
                                @if ((($article->article_type ) === "1A") || (($article->article_type ) === "1B") || (($article->article_type ) === "1C"))
                                    Market News
                                @elseif ((($article->article_type ) === "2A"))
                                    Market Outlook
                                @elseif ((($article->article_type ) === "3A") || (($article->article_type ) === "3B") || (($article->article_type ) === "3C"))
                                    Stock Ideas
                                @elseif ((($article->article_type ) === "4A"))
                                    Broker
                                @elseif ((($article->article_type ) === "5A") || (($article->article_type ) === "5B"))
                                    Funds
                                @elseif ((($article->article_type ) === "6A"))
                                    IR Presentation
                                @endif
                            </td>
                            <td>{{ $article->title }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('admin/article-request/edit/'.$article->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/pages/tablesDatatables.js') }}"></script>

    <script>$(function(){ Index2.init(); TablesDatatables.init(); });</script>
@endsection