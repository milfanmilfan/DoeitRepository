@extends('layouts.admin.index')

@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="gi gi-cogwheels"></i>Article Request<br><small>List of Pending Article</small>
                </h1>
            </div>
        </div>
        @include('layouts.admin.core.alert')
        @include('layouts.admin.core.validate')
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <dl class="dl-horizontal">
                        <dt>Author Profile</dt>
                        <dd>&nbsp;</dd>
                        <dt>Author</dt>
                        <dd>{{ $article->author->name }}</dd>
                        <dt>Email</dt>
                        <dd>{{ $article->author->email }}</dd>
                    </dl>
                    <p>&nbsp;</p>
                    <form action="{{ route('admin.article_request.update', $article->id) }}" method="post" class="" autocomplete="off">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="{{ $article->title }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="article_type">Article Type</label>
                                    <select id="example-chosen" name="article_type" class="select-chosen" data-placeholder="Choose a Article Type.." style="width: 250px;">
                                        <option></option>
                                        <optgroup label="Market News">
                                            <option value="1A" @if($article->article_type == '1A') selected="" @endif>Indonesian Economy</option>
                                            <option value="1B" @if($article->article_type == '1B') selected="" @endif>International Economy</option>
                                            <option value="1C" @if($article->article_type == '1C') selected="" @endif>Company Announcement</option>
                                        </optgroup>
                                        <optgroup label="Market Outlook">
                                            <option value="2A" @if($article->article_type == '2A') selected="" @endif>News</option>
                                        </optgroup>
                                        <optgroup label="Stock Ideas">
                                            <option value="3A" @if($article->article_type == '3A') selected="" @endif>Buy Ideas</option>
                                            <option value="3B" @if($article->article_type == '3B') selected="" @endif>Sell Ideas</option>
                                            <option value="3C" @if($article->article_type == '3C') selected="" @endif>Sectors</option>
                                        </optgroup>
                                        <optgroup label="Broker">
                                            <option value="4A" @if($article->article_type == '4A') selected="" @endif>News</option>
                                        </optgroup>
                                        <optgroup label="Funds">
                                            <option value="5A" @if($article->article_type == '5A') selected="" @endif>Index Funds</option>
                                            <option value="5B" @if($article->article_type == '5B') selected="" @endif>Active Funds</option>
                                        </optgroup>
                                        <optgroup label="IR Presentation">
                                            <option value="6A" @if($article->article_type == '6A') selected="" @endif>News</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Article Summray</label>
                                    <textarea id="summary" name="summary" rows="9" class="form-control" placeholder="Summary">{{ $article->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Article Content</label>
                                    <div class="col-xs-12">
                                        <textarea id="article_content" name="article_content" class="ckeditor">{!! $article->article !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-actions">
                                    <button type="submit" name="btn_save" value="publish" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Save & Publish</button>
                                    <button type="submit" name="btn_save" value="reject" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Reject</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin_asset/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin_asset/js/plugins.js') }}"></script>
    <script src="{{ asset('admin_asset/js/app.js') }}"></script>
    <script src="{{ asset('admin_asset/js/helpers/ckeditor/ckeditor.js') }}"></script>
@endsection