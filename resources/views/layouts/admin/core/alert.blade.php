@if(Session::has('message'))
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="fa fa-check sign"></i><strong> Berhasil!</strong> {{ Session::get('message') }}.
 	</div>
@endif

@if(Session::has('message-error'))
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="fa fa-times-circle sign"></i> <strong> Oopss!</strong> {{ Session::get('message-error') }}.
 	</div>
@endif