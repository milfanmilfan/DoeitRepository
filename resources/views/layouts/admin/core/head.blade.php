<meta charset="utf-8">
<title>Doeit Web Administrator</title>
<meta name="description" content="">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
<link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}">
<link rel="stylesheet" href="{{ asset('admin_asset/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_asset/css/plugins.css') }}">
<link rel="stylesheet" href="{{ asset('admin_asset/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('admin_asset/css/themes.css') }}">
<script src="{{ asset('admin_asset/js/vendor/modernizr.min.js') }}"></script>