<header class="navbar navbar-default navbar-fixed-top">
    <ul class="nav navbar-nav-custom pull-right">
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('admin_asset/img/placeholders/avatars/avatar2.jpg') }}" alt="avatar"> <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                <li class="dropdown-header text-center">Account</li>
                <li>
                    <a href="">
                        <i class="fa fa-user fa-fw pull-right"></i>
                        Profile
                    </a>
                    <a href="#modal-user-settings" data-toggle="modal">
                        <i class="fa fa-cog fa-fw pull-right"></i>
                        Settings
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="login.html"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
    <div id="horizontal-menu-collapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li>
                <a href="" class="sidebar-brand">
                    <img src="{{ asset('web/img/logo.png') }}" alt="" class="logo">
                </a>
            </li>
            <li class="{{ (Request::is('admin/dashboard') ? 'currents' : '') }}">
                <a href="{{ url('admin/dashboard') }}">Home</a>
            </li>
            <li class="{{ (Request::is('admin/article-request') ? 'currents' : '') }}">
                <a href="{{ url('admin/article-request') }}">Article Request</a>
            </li>
            <li class="{{ (Request::is('admin/author-request') ? 'currents' : '') }}">
                <a href="{{ url('admin/author-request') }}">Author Request</a>
            </li>
            <li class="dropdown {{ (Request::is('admin/published-article') ? 'currents' : Request::is('admin/rejected-article') ? 'currents' : Request::is('admin/suspends-article') ? 'currents' : '') }}">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Manage Article <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ url('admin/published-article') }}"><i class="fa fa-book fa-fw pull-right"></i> Published Article</a></li>
                    <li><a href="{{ url('admin/rejected-article') }}"><i class="fa fa-book fa-fw pull-right"></i> Rejected Article</a></li>
                    <li><a href="{{ url('admin/suspends-article') }}"><i class="fa fa-book fa-fw pull-right"></i> Suspend Article</a></li>
                </ul>
            </li>
            <li class="dropdown {{ (Request::is('admin/member') ? 'currents' : Request::is('admin/intern-author') ? 'currents' : Request::is('admin/extern-author') ? 'currents' : '') }}">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">List of Data <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ url('admin/member') }}"><i class="fa fa-user fa-fw pull-right"></i> Member</a></li>
                    <li><a href="{{ url('admin/intern-author') }}"><i class="fa fa-user fa-fw pull-right"></i> Intern Author</a></li>
                    <li><a href="{{ url('admin/extern-author') }}"><i class="fa fa-user fa-fw pull-right"></i> Extern Author</a></li>
                </ul>
            </li>
        </ul>
    </div>
</header>