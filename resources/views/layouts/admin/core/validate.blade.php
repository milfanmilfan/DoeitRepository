@if ($errors->count() > 0)
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><strong>Oops!</strong></h4>
		@foreach ($errors->all(':message') as $error)
            <p><i class="fa fa-times-circle sign"></i> {{ $error }}</p>
    	@endforeach
		
 	</div>
@endif