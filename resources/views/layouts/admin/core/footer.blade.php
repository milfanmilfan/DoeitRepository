<footer class="clearfix">
    <div class="pull-right">
        Powered By  <a href="http://alphaboarddev.com/" target="_blank">AlphaboardDEV</a>
    </div>
    <div class="pull-left">
        2017 &copy; <a href="http://doeit.org/" target="_blank">Doeit.org</a>
    </div>
</footer>