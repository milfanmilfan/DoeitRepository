<aside class="sidebar col-lg-3 col-md-4 col-sm-4 col-md-pull-8 col-lg-pull-9">
    <div id="nav_menu-5" class="widget_nav_menu">
        <div class="menu-side-navigation-container">
            <ul id="menu-side-navigation" class="menu">
              <li class="menu-item {{ (Request::is('authors/manage-profile/personal-detail') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-profile/personal-detail') }}">Your Profile</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-profile/interest') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-profile/interest') }}">Your Interest</a></li>
            </ul>
        </div>
    </div>
</aside>
