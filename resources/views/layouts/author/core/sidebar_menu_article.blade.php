<aside class="sidebar col-lg-3 col-md-4 col-sm-4 col-md-pull-8 col-lg-pull-9">
    <div id="nav_menu-5" class="widget_nav_menu">
        <div class="menu-side-navigation-container">
            <ul id="menu-side-navigation" class="menu">
                <li class="menu-item {{ (Request::is('authors/manage-article/all') ? 'current-menu-item page_item current_page_item' : Request::is('authors/manage-article') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/all') }}">All Article ({{ $all_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/market-news') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/market-news') }}">Market News ({{ $market_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/market-outlook') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/market-outlook') }}">Market Outlook ({{ $marketoutlook_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/stockideas') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/stockideas') }}">Stock Ideas ({{ $stockideas_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/IR-presentation') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/IR-presentation') }}">IR Presentation ({{ $irpresentation_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/broker-report') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/broker-report') }}">Broke Report ({{ $broker_count }})</a></li>
                <li class="menu-item {{ (Request::is('authors/manage-article/funds') ? 'current-menu-item page_item current_page_item' : '') }}"><a href="{{ url('authors/manage-article/funds') }}">Funds ({{ $funds_count }})</a></li>
            </ul>
        </div>
    </div>
</aside>
