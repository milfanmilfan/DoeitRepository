<ul id="main-nav" class="menu">
    <li class="menu-item"><a href="{{ url('/') }}">Home</a></li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">Market News</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('market-news/all') }}">All</a>
            <li class="menu-item"><a href="{{ url('market-news/indonesian-economy') }}">Indonesian Economy</a>
            <li class="menu-item"><a href="{{ url('market-news/international-economy') }}">International Economy</a>
            <li class="menu-item"><a href="{{ url('market-news/company-announcement') }}">Company Announcement</a>
        </ul>
    </li>
    <li class="menu-item"><a href="{{ url('market-outlook') }}">Market Outlook</a></li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">Stock Ideas</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('stock-ideas/buy-ideas') }}">Buy Ideas</a>
            <li class="menu-item"><a href="{{ url('stock-ideas/sell-ideas') }}">Sell Ideas</a>
            <li class="menu-item"><a href="{{ url('stock-ideas/sectors') }}">Sectors</a>
        </ul>
    </li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">Stock Ideas</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('stock-ideas/buy-ideas') }}">Buy Ideas</a>
            <li class="menu-item"><a href="{{ url('stock-ideas/sell-ideas') }}">Sell Ideas</a>
            <li class="menu-item"><a href="{{ url('stock-ideas/sectors') }}">Sectors</a>
        </ul>
    </li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">Discount</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('discount/regional') }}">Regional</a>
            <li class="menu-item"><a href="{{ url('discount/newest') }}">Newest</a>
            <li class="menu-item"><a href="{{ url('discount/likes') }}">Likes</a>
        </ul>
    </li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">IR Presentation</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('IR-presentation/latest') }}">Latest</a>
            <li class="menu-item"><a href="{{ url('IR-presentation/search-within-ir') }}">Search Within IR</a>
        </ul>
    </li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="{{ url('broker-report') }}">Broker Report</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="">All</a>
            <li class="menu-item"><a href="">Indonesian Economy</a>
            <li class="menu-item"><a href="">International Economy</a>
            <li class="menu-item"><a href="">Company Announcement</a>
        </ul>
    </li>
    <li class="menu-item current-menu-ancestor current_page_ancestor menu-item-has-children"><a href="#">Funds</a>
        <ul class="sub-menu">
            <li class="menu-item"><a href="{{ url('funds/index-funds') }}">Index Funds</a>
            <li class="menu-item"><a href="{{ url('funds/active-funds') }}">Active Funds</a>
        </ul>
    </li>
    {{--<li class="menu-item"><a href="{{ url('pro') }}"><strong>PRO</strong></a></li>--}}
    <li class="menu-item"><a href="{{ url('marketplace') }}"><strong>Marketplace</strong></a></li>
</ul>