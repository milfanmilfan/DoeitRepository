<footer id="footer" class="footer-2">
    <div id="main-footer" class="smallest-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div id="text-2" class="widget widget_text">
                        <div class="textwidget txt-left">
                            <img src="{{ asset('web/img/logo.png') }}" alt="logo">
                            <p>
                                Vivamus orci sem, consectetur ut vestibulum a, mper ac dui. Aenean tellus nisl, commodo eu aliquet ut, pulvinar ut sapien. Proin tate aliquam mi nec hendrerit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <h4>Social Media</h4>
                    <div id="social-media-2" class="widget widget_social_media txt-center" >
                        <ul class="social-media">
                            <li class="tooltip-ontop" title="Facebook"><a href="dfsdfsdfscvc"><i class="icon-facebook"></i></a></li>
                            <li class="tooltip-ontop" title="Twitter"><a href="dfsder"><i class="icon-twitter"></i></a></li>
                            <li class="tooltip-ontop" title="Skype"><a href="fsdf"><i class="icon-skype"></i></a></li>
                            <li class="tooltip-ontop" title="Google Plus"><a href="sdfs"><i class="icon-google"></i></a></li>
                            <li class="tooltip-ontop" title="Vimeo"><a href="dfsdf"><i class="icon-vimeo"></i></a></li>
                            <li class="tooltip-ontop" title="Linkedin"><a href="fsdfsd"><i class="icon-linkedin"></i></a></li>
                            <li class="tooltip-ontop" title="Instagram"><a href="fsdfsdf"><i class="icon-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 pull-right txt-right">
                    <h5>
                        <a href="" class="font-dark">Top Author |</a>
                        <a href="" class="font-dark">Sitemap | </a>
                        <a href="" class="font-dark">About Us | </a>
                        <a href="" class="font-dark">Contact Us</a>
                    </h5>
                    <a href="" class="font-dark">Term of Use | </a>
                    <a href="" class="font-dark">Privacy & Policy </a>
                </div>
            </div>
        </div>
    </div>
    <div id="lower-footer">
        <div class="container">
            <span class="copyright">© 2017 Doeit.org. All Rights Reserved</span>
        </div>
    </div>
</footer>