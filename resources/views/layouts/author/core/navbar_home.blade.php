<ul id="main-nav" class="menu pull-right">
    <li class="menu-item"><a href="{{ url('/authors') }}"  class="{{ (Request::is('authors') ? 'current' : '') }}" >Home</a></li>
    <li class="menu-item"><a href="{{ url('/authors/write-article') }}"  class="{{ (Request::is('authors/write-article') ? 'current' : '') }}" >Write Article</a></li>
    <li class="menu-item"><a href="{{ url('/authors/manage-article/all') }}"  class="{{ (Request::is('authors/manage-article/all') ? 'current' : (Request::is('authors/manage-article/market-news') ? 'current' : (Request::is('authors/manage-article/market-outlook') ? 'current' : (Request::is('authors/manage-article/stockideas') ? 'current' : (Request::is('authors/manage-article/IR-presentation') ? 'current' : (Request::is('authors/manage-article/funds') ? 'current' : (Request::is('authors/manage-article/broker-report') ? 'current' : ''))))))) }}" >Manage Articles</a></li>
    <li class="menu-item"><a href="{{ url('/authors/manage-article-comments') }}"  class="{{ (Request::is('authors/manage-article-comments') ? 'current' : '') }}" >Manage Article Comment</a></li>
    <li class="menu-item"><a href="{{ url('/authors/profile') }} " class="{{ (Request::is('authors/profile') ? 'current' : (Request::is('authors/manage-profile/interest') ? 'current' : (Request::is('authors/manage-profile/personal-detail') ? 'current' : ''))) }}">Manage Profile</a></li>
    {{--<li class="menu-item"><a href="{{ url('/authors/mail-box') }}"  class="{{ (Request::is('authors/mail-box') ? 'current' : '') }}" >Mail Box</a></li>--}}
</ul>
