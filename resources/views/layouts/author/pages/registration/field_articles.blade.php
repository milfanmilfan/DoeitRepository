<fieldset>
    <div class="row">
        <div class="col-md-12">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3"><strong>Your Articles Profile</strong></h3>
                    <span style="font-size: 14pt;">The information entered in this section will be visible on every article you write</span><br>
                </li>
                <li class="marine-header6-serviceslist-li">
                    &nbsp;
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <ul class="services-list">
                        <li class="marine-header6-serviceslist-li">
                            <h3 class="marine-heade6-services-h3">Profile Picture</h3>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="team-member " data-animation="">
                        <a href="#" title="profile Picture" id="div-image">
                            <img src="{{ asset('web/img/profile/570x543.png') }}" alt="img-responsive"/>
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="iconic-input">
                                <input type="file" class="form-control" value="" accept="image/*" id="profile_pict" name="profile_pict">
                            </div>
                        </div>
                        <div class="col-md-6">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span style="text-align: left !important;">Upload a headshot of yourself or an invesment related images.</span>
                            <br>
                            <span style="text-align: left !important;">If youare using a pen name, please don't use a headshot.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
    </div>
    <div class="row" >
        <div class="col-md-6">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Your Sitename & URL</h3>
                    {{--<span>(optional)</span>--}}
                </li>
                <li>
                    A link to your personal site, your firms's website or any professional site that will help readers to learn more about you.
                    <br><br>
                    <div class="iconic-input">
                        <span>Site Name :</span>
                        <input type="text" class="form-control" value="{{ old('sitename') }}" name="sitename">
                    </div>
                    <div class="iconic-input">
                        <span>Site URL :</span>
                        <input type="text" class="form-control" value="{{ old('siteurl') }}" name="siteurl">
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Mini Bio</h3>
                </li>
                <li>
                    Please select up to four choices that best describe your investing style, profession and areas of expertise.
                    <br>
                    Choices will appear in the order they're selected. To change the order, unselect and reselect.
                    <br><br>
                    <div class="iconic-input">
                        <span>Your Mini Bio :</span>
                        <textarea class="form-control" name="minibio" rows="3">{{ old('minibio') }}</textarea>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="f1-buttons pull-right">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
            </div>
        </div>
    </div>
</fieldset>
