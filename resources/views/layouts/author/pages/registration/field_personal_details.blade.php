<fieldset>
    <div class="row">
        <div class="col-md-12">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3"><strong>Personal Detail</strong></h3>
                    <span style="font-size: 14pt;">Your personal information is for our internal records only.</span><br>
                    <span>Your personal details will be verified. Please take care to enter this information accurately, as errors will delay your registration and could lead to a rejection of your application. You agree to only maintain a single account on Doeit</span>
                </li>
                <li class="marine-header6-serviceslist-li">
                    &nbsp;
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <div class="iconic-input" style="margin-bottom: 10px;">
                        <span><strong>Date of Birth</strong> (You must be at least 18 years old to register)</span><br>
                        <input type="text" class="form-control datepicker" value="{{ old('datebirth') }}" name="datebirth" readonly="" style="cursor: pointer;">
                    </div>
                    <div class="iconic-input">
                        <span>First Name :</span>
                        <input type="text" class="form-control" value="{{ old('firstname') }}" name="firstname">
                    </div>
                    <div class="iconic-input">
                        <span>Last Name :</span>
                        <input type="text" class="form-control" value="{{ old('lastname') }}" name="lastname">
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <div class="iconic-input">
                        <span>Address :</span>
                        <textarea class="form-control" name="address" rows="3">{{ old('address') }}</textarea>
                    </div>
                    <div class="iconic-input">
                        <span>City :</span>
                        <input type="text" class="form-control" value="{{ old('city') }}" name="city">
                    </div>
                    <div class="iconic-input">
                        <span>Postal Code :</span>
                        <input type="text" class="form-control" value="{{ old('postalcode') }}" era-numeric="true" name="postalcode">
                    </div>
                    <div class="iconic-input">
                        <span>Phone Number :</span>
                        <input type="text" class="form-control" value="{{ old('phone') }}" era-numeric="true" name="phone">
                    </div>
                    {{-- <div class="iconic-input">
                        <span>Email Address :</span>
                        <input type="email" class="form-control" value="" name="email">
                    </div> --}}
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="f1-buttons pull-right">
                <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
            </div>
        </div>
    </div>
</fieldset>
