<fieldset>
    <div class="row">
        <div class="col-md-12">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3"><strong>Your Author Page</strong></h3>
                    <span style="font-size: 14pt;">The information entered in this section will be visible on your page.</span><br>
                </li>
                <li class="marine-header6-serviceslist-li">
                    &nbsp;
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Financial Advisor</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1A" name="category[]" @if(is_array(old('category')) && in_array(1, old('category'))) checked @endif />
                        <span>Registered Investing Advisor (RIA)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2A" name="category[]" @if(is_array(old('category')) && in_array(2, old('category'))) checked @endif >
                        <span>Certified Financial Planner (CFP)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3A" name="category[]" @if(is_array(old('category')) && in_array(3, old('category'))) checked @endif>
                        <span>Licensed Securities Brokers</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4A" name="category[]" @if(is_array(old('category')) && in_array(4, old('category'))) checked @endif>
                        <span>Insurance Brokers</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5A" name="category[]" @if(is_array(old('category')) && in_array(5, old('category'))) checked @endif>
                        <span>Accountant</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Hedge Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1B" name="category[]" @if(is_array(old('category')) && in_array(6, old('category'))) checked @endif/>
                        <span>Hedge Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2B" name="category[]" @if(is_array(old('category')) && in_array(7, old('category'))) checked @endif>
                        <span>Hedge Fund Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3B" name="category[]" @if(is_array(old('category')) && in_array(8, old('category'))) checked @endif>
                        <span>Hedge Fund Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4B" name="category[]" @if(is_array(old('category')) && in_array(9, old('category'))) checked @endif>
                        <span>Hedge Fund Director of Research</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5B" name="category[]" @if(is_array(old('category')) && in_array(10, old('category'))) checked @endif>
                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Mutual Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1C" name="category[]" @if(is_array(old('category')) && in_array(11, old('category'))) checked @endif />
                        <span>Mutual Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2C" name="category[]" @if(is_array(old('category')) && in_array(12, old('category'))) checked @endif >
                        <span>Mutual Fund Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3C" name="category[]" @if(is_array(old('category')) && in_array(13, old('category'))) checked @endif>
                        <span>Mutual Fund Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4C" name="category[]" @if(is_array(old('category')) && in_array(14, old('category'))) checked @endif>
                        <span>Mutual Fund Sales</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5C" name="category[]" @if(is_array(old('category')) && in_array(15, old('category'))) checked @endif>
                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Pension Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1D" name="category[]" @if(is_array(old('category')) && in_array(16, old('category'))) checked @endif />
                        <span>Pension Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2D" name="category[]" @if(is_array(old('category')) && in_array(17, old('category'))) checked @endif >
                        <span>Pension Fund Analyst</span>
                    </div>
                </li>

            </ul>
        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Endowment Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1E" name="category[]" @if(is_array(old('category')) && in_array(18, old('category'))) checked @endif />
                        <span>Endowment Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2E" name="category[]" @if(is_array(old('category')) && in_array(19, old('category'))) checked @endif >
                        <span>Endowment Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Prive Equiy</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1F" name="category[]" @if(is_array(old('category')) && in_array(20, old('category'))) checked @endif />
                        <span>Prive Equity Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2F" name="category[]" @if(is_array(old('category')) && in_array(21, old('category'))) checked @endif >
                        <span>Prive Equity Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Venture Capital</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1G" name="category[]" @if(is_array(old('category')) && in_array(22, old('category'))) checked @endif />
                        <span>Venture Capital Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2G" name="category[]" @if(is_array(old('category')) && in_array(23, old('category'))) checked @endif >
                        <span>Venture Capital Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Company Executive</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1H" name="category[]" @if(is_array(old('category')) && in_array(24, old('category'))) checked @endif />
                        <span>CEO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2H" name="category[]" @if(is_array(old('category')) && in_array(25, old('category'))) checked @endif >
                        <span>CFO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3H" name="category[]" @if(is_array(old('category')) && in_array(26, old('category'))) checked @endif >
                        <span>COO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4H" name="category[]" @if(is_array(old('category')) && in_array(27, old('category'))) checked @endif >
                        <span>Board Member</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5H" name="category[]" @if(is_array(old('category')) && in_array(28, old('category'))) checked @endif >
                        <span>Small Bussiness Owner</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6H" name="category[]" @if(is_array(old('category')) && in_array(29, old('category'))) checked @endif >
                        <span>Other C-Level Excetive</span>
                    </div>
                </li>
            </ul>

        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Other Financials</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1I" name="category[]" @if(is_array(old('category')) && in_array(30, old('category'))) checked @endif />
                        <span>Investment Consultan</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2I" name="category[]" @if(is_array(old('category')) && in_array(31, old('category'))) checked @endif >
                        <span>Investment Banker/Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3I" name="category[]" @if(is_array(old('category')) && in_array(32, old('category'))) checked @endif >
                        <span>Family Office Manager/Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4I" name="category[]" @if(is_array(old('category')) && in_array(33, old('category'))) checked @endif >
                        <span>Sell Side Research Analyst/Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5I" name="category[]" @if(is_array(old('category')) && in_array(34, old('category'))) checked @endif >
                        <span>Commecial Banker</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6I" name="category[]" @if(is_array(old('category')) && in_array(35, old('category'))) checked @endif >
                        <span>Operation Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7I" name="category[]" @if(is_array(old('category')) && in_array(36, old('category'))) checked @endif >
                        <span>Complience Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="8I" name="category[]" @if(is_array(old('category')) && in_array(37, old('category'))) checked @endif >
                        <span>IR Professional</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Individual Investor</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1J" name="category[]" @if(is_array(old('category')) && in_array(38, old('category'))) checked @endif />
                        <span>Full-time Investor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2J" name="category[]" @if(is_array(old('category')) && in_array(39, old('category'))) checked @endif >
                        <span>Occasional Investor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3J" name="category[]" @if(is_array(old('category')) && in_array(40, old('category'))) checked @endif >
                        <span>Retiree</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Academic / Student (Current)</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1K" name="category[]" @if(is_array(old('category')) && in_array(41, old('category'))) checked @endif />
                        <span>High School Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2K" name="category[]" @if(is_array(old('category')) && in_array(42, old('category'))) checked @endif >
                        <span>Undergraduated Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3K" name="category[]" @if(is_array(old('category')) && in_array(43, old('category'))) checked @endif >
                        <span>Chartered Financial Analyst (CFA)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4K" name="category[]" @if(is_array(old('category')) && in_array(44, old('category'))) checked @endif >
                        <span>MBA Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5K" name="category[]" @if(is_array(old('category')) && in_array(45, old('category'))) checked @endif >
                        <span>Other Graduate Program</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6K" name="category[]" @if(is_array(old('category')) && in_array(46, old('category'))) checked @endif >
                        <span>Doctoral Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7K" name="category[]" @if(is_array(old('category')) && in_array(47, old('category'))) checked @endif >
                        <span>Profesor</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Other Non-Financial Professional</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1L" name="category[]" @if(is_array(old('category')) && in_array(48, old('category'))) checked @endif />
                        <span>PR Professional</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2L" name="category[]" @if(is_array(old('category')) && in_array(49, old('category'))) checked @endif >
                        <span>Lawyer</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3L" name="category[]" @if(is_array(old('category')) && in_array(50, old('category'))) checked @endif >
                        <span>Doctor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4L" name="category[]" @if(is_array(old('category')) && in_array(51, old('category'))) checked @endif >
                        <span>Writer</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5L" name="category[]" @if(is_array(old('category')) && in_array(52, old('category'))) checked @endif >
                        <span>Journalist</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6L" name="category[]" @if(is_array(old('category')) && in_array(53, old('category'))) checked @endif >
                        <span>Professional Blogger</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7L" name="category[]" @if(is_array(old('category')) && in_array(54, old('category'))) checked @endif >
                        <span>Newsletter Author</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="8L" name="category[]" @if(is_array(old('category')) && in_array(55, old('category'))) checked @endif >
                        <span>Other</span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="f1-buttons">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-submit">Submit</button>
            </div>
        </div>
    </div>
</fieldset>