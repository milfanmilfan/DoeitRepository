@extends('layouts.author.index_home')

@section('style')

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href="{{ asset('web/css/bootstrap.min.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href="{{ asset('web/css/fontello.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyphoto-css-css' href="{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href="{{ asset('web/css/animation.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href="{{ asset('web/css/flexslider.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href="{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href="{{ asset('web/css/jquery.validity.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href="{{ asset('web/css/jquery-ui.min.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href="{{ asset('web/css/style.css') }}" type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href="{{ asset('web/css/mobilenav.css') }}" type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/css/style.css') }}">

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="container">
            <div class="row">
                <section class="main-content col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div id="post-items">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <ul class="services-list">
                                        <li class="marine-header6-serviceslist-li" style="text-align: left; margin-top: 20px;">
                                            <h3 class="marine-heade6-services-h3"><strong>Manage Profile</strong></h3>
                                        </li>
                                        <li class="marine-header6-serviceslist-li">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: left !important;">
                                <section class="col-lg-9 col-md-8 col-sm-8 col-md-push-4 col-lg-push-3">
                                  <div class="col-md-12">
                                      <ul class="services-list">
                                          <li class="marine-header6-serviceslist-li">
                                                <h3 class="marine-heade6-services-h3"><strong>Your Interest</strong></h3>
                                          </li>
                                          <li class="marine-header6-serviceslist-li">
                                              &nbsp;
                                          </li>
                                      </ul>
                                  </div>
                                  <div class="col-md-12">
                                    <form role="form" method="post" id="regisform" enctype="multipart/form-data" autocomplete="off">
                                      
                                      {!! csrf_field() !!}
                                      {!! method_field('PATCH') !!}
                                      <input type="hidden" class="has_change" name="has_change" value="0">

                                      <div class="row">
                                        <div class="col-md-4">
                                            <ul class="services-list">
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Financial Advisor</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1A" name="category[]" @if($data->where('interest_value', '1A')->toArray()) checked @endif />
                                                        <span>Registered Investing Advisor (RIA)</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2A" name="category[]" @if($data->where('interest_value', '2A')->toArray()) checked @endif >
                                                        <span>Certified Financial Planner (CFP)</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3A" name="category[]" @if($data->where('interest_value', '3A')->toArray()) checked @endif>
                                                        <span>Licensed Securities Brokers</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4A" name="category[]" @if($data->where('interest_value', '4A')->toArray()) checked @endif>
                                                        <span>Insurance Brokers</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5A" name="category[]" @if($data->where('interest_value', '5A')->toArray()) checked @endif>
                                                        <span>Accountant</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Hedge Funds</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1B" name="category[]" @if($data->where('interest_value', '1B')->toArray()) checked @endif/>
                                                        <span>Hedge Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2B" name="category[]" @if($data->where('interest_value', '2B')->toArray()) checked @endif>
                                                        <span>Hedge Fund Analyst</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3B" name="category[]" @if($data->where('interest_value', '3B')->toArray()) checked @endif>
                                                        <span>Hedge Fund Trader</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4B" name="category[]" @if($data->where('interest_value', '4B')->toArray()) checked @endif>
                                                        <span>Hedge Fund Director of Research</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5B" name="category[]" @if($data->where('interest_value', '5B')->toArray()) checked @endif>
                                                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Mutual Funds</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1C" name="category[]" @if($data->where('interest_value', '1C')->toArray()) checked @endif />
                                                        <span>Mutual Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2C" name="category[]" @if($data->where('interest_value', '2C')->toArray()) checked @endif >
                                                        <span>Mutual Fund Analyst</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3C" name="category[]" @if($data->where('interest_value', '3C')->toArray()) checked @endif>
                                                        <span>Mutual Fund Trader</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4C" name="category[]" @if($data->where('interest_value', '4C')->toArray()) checked @endif>
                                                        <span>Mutual Fund Sales</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5C" name="category[]" @if($data->where('interest_value', '5C')->toArray()) checked @endif>
                                                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Pension Funds</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1D" name="category[]" @if($data->where('interest_value', '1D')->toArray()) checked @endif />
                                                        <span>Pension Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2D" name="category[]" @if($data->where('interest_value', '2D')->toArray()) checked @endif >
                                                        <span>Pension Fund Analyst</span>
                                                    </div>
                                                </li>


                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="services-list">
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Endowment Funds</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1E" name="category[]" @if($data->where('interest_value', '1E')->toArray()) checked @endif />
                                                        <span>Endowment Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2E" name="category[]" @if($data->where('interest_value', '2E')->toArray()) checked @endif >
                                                        <span>Endowment Fund Analyst</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Prive Equiy</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1F" name="category[]" @if($data->where('interest_value', '1F')->toArray()) checked @endif />
                                                        <span>Prive Equity Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2F" name="category[]" @if($data->where('interest_value', '2F')->toArray()) checked @endif >
                                                        <span>Prive Equity Fund Analyst</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Venture Capital</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1G" name="category[]" @if($data->where('interest_value', '1G')->toArray()) checked @endif />
                                                        <span>Venture Capital Fund Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2G" name="category[]" @if($data->where('interest_value', '2G')->toArray()) checked @endif >
                                                        <span>Venture Capital Fund Analyst</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Company Executive</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1H" name="category[]" @if($data->where('interest_value', '1H')->toArray()) checked @endif />
                                                        <span>CEO</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2H" name="category[]" @if($data->where('interest_value', '2H')->toArray()) checked @endif >
                                                        <span>CFO</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3H" name="category[]" @if($data->where('interest_value', '3H')->toArray()) checked @endif >
                                                        <span>COO</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4H" name="category[]" @if($data->where('interest_value', '4H')->toArray()) checked @endif >
                                                        <span>Board Member</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5H" name="category[]" @if($data->where('interest_value', '5H')->toArray()) checked @endif >
                                                        <span>Small Bussiness Owner</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="6H" name="category[]" @if($data->where('interest_value', '6H')->toArray()) checked @endif >
                                                        <span>Other C-Level Excetive</span>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-md-4">
                                            <ul class="services-list">
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Other Financials</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1I" name="category[]" @if($data->where('interest_value', '1I')->toArray()) checked @endif />
                                                        <span>Investment Consultan</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2I" name="category[]" @if($data->where('interest_value', '2I')->toArray()) checked @endif >
                                                        <span>Investment Banker/Analyst</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3I" name="category[]" @if($data->where('interest_value', '3I')->toArray()) checked @endif >
                                                        <span>Family Office Manager/Analyst</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4I" name="category[]" @if($data->where('interest_value', '4I')->toArray()) checked @endif >
                                                        <span>Sell Side Research Analyst/Trader</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5I" name="category[]" @if($data->where('interest_value', '5I')->toArray()) checked @endif >
                                                        <span>Commecial Banker</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="6I" name="category[]" @if($data->where('interest_value', '6I')->toArray()) checked @endif >
                                                        <span>Operation Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="7I" name="category[]" @if($data->where('interest_value', '7I')->toArray()) checked @endif >
                                                        <span>Complience Manager</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="8I" name="category[]" @if($data->where('interest_value', '8I')->toArray()) checked @endif >
                                                        <span>IR Professional</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Individual Investor</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1J" name="category[]" @if($data->where('interest_value', '1J')->toArray()) checked @endif />
                                                        <span>Full-time Investor</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2J" name="category[]" @if($data->where('interest_value', '2J')->toArray()) checked @endif >
                                                        <span>Occasional Investor</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3J" name="category[]" @if($data->where('interest_value', '3J')->toArray()) checked @endif >
                                                        <span>Retiree</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Academic / Student (Current)</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1K" name="category[]" @if($data->where('interest_value', '1K')->toArray()) checked @endif />
                                                        <span>High School Student</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2K" name="category[]" @if($data->where('interest_value', '2K')->toArray()) checked @endif >
                                                        <span>Undergraduated Student</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3K" name="category[]" @if($data->where('interest_value', '3K')->toArray()) checked @endif >
                                                        <span>Chartered Financial Analyst (CFA)</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4K" name="category[]" @if($data->where('interest_value', '4K')->toArray()) checked @endif >
                                                        <span>MBA Student</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5K" name="category[]" @if($data->where('interest_value', '5K')->toArray()) checked @endif >
                                                        <span>Other Graduate Program</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="6K" name="category[]" @if($data->where('interest_value', '6K')->toArray()) checked @endif >
                                                        <span>Doctoral Student</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="7K" name="category[]" @if($data->where('interest_value', '7K')->toArray()) checked @endif >
                                                        <span>Profesor</span>
                                                    </div>
                                                </li>
                                                <li class="marine-header6-serviceslist-li">
                                                    <h3 class="marine-heade6-services-h3">Other Non-Financial Professional</h3>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="1L" name="category[]" @if($data->where('interest_value', '1L')->toArray()) checked @endif />
                                                        <span>PR Professional</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="2L" name="category[]" @if($data->where('interest_value', '2L')->toArray()) checked @endif >
                                                        <span>Lawyer</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="3L" name="category[]" @if($data->where('interest_value', '3L')->toArray()) checked @endif >
                                                        <span>Doctor</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="4L" name="category[]" @if($data->where('interest_value', '4L')->toArray()) checked @endif >
                                                        <span>Writer</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="5L" name="category[]" @if($data->where('interest_value', '5L')->toArray()) checked @endif >
                                                        <span>Journalist</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="6L" name="category[]" @if($data->where('interest_value', '6L')->toArray()) checked @endif >
                                                        <span>Professional Blogger</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="7L" name="category[]" @if($data->where('interest_value', '7L')->toArray()) checked @endif >
                                                        <span>Newsletter Author</span>
                                                    </div>
                                                    <div class="iconic-input">
                                                        <input type="checkbox" class="interest-checkbox" value="8L" name="category[]" @if($data->where('interest_value', '8L')->toArray()) checked @endif >
                                                        <span>Other</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                          <button type="submit" class="btn btn-submit pull-right">Update</button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                  <div class="small-padding">
                                    &nbsp;
                                  </div>
                                </section>
                                @include('layouts.author.core.sidebar_menu_profile')
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>

    {{--    <script src="{{ asset('web/wizard_assets/js/jquery-1.11.1.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('web/wizard_assets/bootstrap/js/bootstrap.min.js') }}"></script>--}}
    <script src="{{ asset('web/wizard_assets/js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/scripts.js') }}"></script>

    <script src="{{ asset('web/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web/js/custom.js') }}"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd MM yyyy'
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });

            $(".interest-checkbox").change(function(e){
                $('.has_change').val('1');
            });
        }); //ready
    </script>
    <script type="text/javascript">
    $(document).ready(function(e){
    @if ($errors->count() > 0)
        @foreach ($errors->getMessages() as $key => $message)

            validate_script("{{ $key }}", "{{ $message[0] }}")

        @endforeach
    @endif

        var file = document.getElementById('profile_pict');
        function showPreview()
        {
            for(var i = 0; i < file.files.length; i++){
                var each_file = file.files[i];
                var reader = new FileReader();

                reader.onload = function(e){
                    //$('.show-images').append('<img src="'+e.target.result+'" />');
                    $("#div-image").html('<img src="'+e.target.result+'" class="thumb-img img-thumbnail" style="max-width: 150px; height: auto;">');
                }

                reader.readAsDataURL(each_file);
            }
        }
        $("body").delegate('#profile_pict', 'change', function(e){
             showPreview();
        });

    });
    </script>
@endsection
