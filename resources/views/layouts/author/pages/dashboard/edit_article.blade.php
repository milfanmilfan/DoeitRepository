@extends('layouts.author.index_home')

@section('style')
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/css/style.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="container">
            <div class="row">
                <section class="main-content col-lg-12 col-md-12 col-sm-12 small-padding">
                    <div class="row">
                        <div id="post-items">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <ul class="services-list">
                                        <li class="marine-header6-serviceslist-li" style="text-align: left; margin-top: 20px;">
                                            <h3 class="marine-heade6-services-h3"><strong>Write Your Article</strong></h3>
                                        </li>
                                        <li class="marine-header6-serviceslist-li">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <form enctype="multipart/form-data" role="form" method="post" class="f1 get-in-touch light">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                    <div class="f1-steps" style="margin-bottom: 20px;">
                                        <div class="f1-progress">
                                            <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                                        </div>
                                        <div class="f1-step active">
                                            <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                                            <p>Choose Type</p>
                                        </div>
                                        <div class="f1-step">
                                            <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                                            <p>Write Article</p>
                                        </div>
                                        <div class="f1-step">
                                            <div class="f1-step-icon"><i class="fa fa-twitter"></i></div>
                                            <p>Tagging & Disclosure</p>
                                        </div>
                                    </div>

                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="services-list">
                                                    <li class="marine-header6-serviceslist-li">
                                                        <h3 class="marine-heade6-services-h3">What do you want to write ?</h3>
                                                    </li>
                                                    <li class="marine-header6-serviceslist-li">
                                                        &nbsp;
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-12 masonry-box">
                                                <div class="blog-post masonry">
                                                    <div class="post">
                                                        <div class="post-content">
                                                            <div class="iconic-input">
                                                                <input type="radio" value="1" name="article_category" @if($data->article_category == 1) checked @endif />
                                                                <span style="font-size: 15pt; margin-left: 10px;"><strong>An Article that can be published only Doeit.org</strong></span>
                                                                <p style="margin-left: 20px;">
                                                                    You are eligble to earn up to $500 for your published articles
                                                                    <br>
                                                                    By clicking 'Next', you agree to the Term & Conditions of Doeit's Contributor Partnership Program and to Doeit general Term of Use.
                                                                    Articles must be accepted by our editor for publication. Once published, the articles must be exclusive to Doeit
                                                                </p>
                                                                <p style="margin-left: 20px;">

                                                                </p>
                                                            </div>
                                                            <div class="iconic-input">
                                                                <input type="radio" value="2" name="article_category" @if($data->article_category == 2) checked @endif/>
                                                                <span style="font-size: 15pt; margin-left: 10px;"><strong>An Article that can be published on other sites</strong></span>
                                                                <p style="margin-left: 20px;">
                                                                    Your article will be published after it is accepted by our editors
                                                                </p>
                                                            </div>
                                                            <div class="iconic-input">
                                                                <input type="radio" value="3" name="article_category" @if($data->article_category == 3) checked @endif />
                                                                <span style="font-size: 15pt; margin-left: 10px;"><strong>An Instablog Post</strong></span>
                                                                <p style="margin-left: 20px;">
                                                                    Your personal blog for anything stock market related
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post-footer">
                                                        <span>if you experience any technical difficulties submitting an article, please send a copy of the article with brief explanation and fll disclosure to submission@doeit.org</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">&nbsp;</div>
                                            <div class="col-md-12">
                                                <div class="f1-buttons pull-right">
                                                    <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="services-list">
                                                            <li class="marine-header6-serviceslist-li">
                                                                <h3 class="marine-heade6-services-h3">Write Your Article</h3>
                                                            </li>
                                                            <li class="marine-header6-serviceslist-li">
                                                                &nbsp;
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="iconic-input">
                                                                    <label for="">Title :</label>
                                                                    <input type="text" class="form-control" value="{{ $data->title }}" name="title" placeholder="Enter your title here">
                                                                </div>

                                                                <div class="iconic-input">
                                                                    <label for="">Description :</label>
                                                                    <textarea name="description" id="description" cols="30" rows="20">{!! $data->description !!}</textarea>
                                                                    {{--<input type="text" class="form-control" value="" name="description" placeholder="Enter your article description">--}}
                                                                </div>

                                                                <div class="iconic-input">
                                                                    <label for="">Your Article :</label>
                                                                    <textarea name="article" id="article" cols="30" rows="20">{!! $data->article !!}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="f1-buttons pull-right">
                                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                                    <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="blog-post masonry">
                                                            <div class="post">
                                                                <div class="post-content">
                                                                    <h3 class="marine-heade6-services-h3">When choosing a title :</h3>
                                                                    <ul class="list wow animated" style="margin-left: 30px;">
                                                                        <li class="icon-minus" style="text-indent: -2.3em;">
                                                                            On the web, readers use title to decide what they should read. Try to express, succintly and impactfully, the article's key thrust. Whats set it apart ?
                                                                        </li>
                                                                        <li class="icon-minus" style="text-indent: -2.3em;">
                                                                            Try not to use questions in titles unless you plan to answer the question
                                                                        </li>
                                                                        <li class="icon-minus" style="text-indent: -2.3em;">
                                                                            Sensationalistic and bombastic titles may be rejected
                                                                        </li>
                                                                    </ul>
                                                                    <h3 class="marine-heade6-services-h3">When writing an article :</h3>
                                                                    <ul class="list wow animated" style="margin-left: 30px;">
                                                                        <li class="icon-minus" style="text-indent: -2.3em;">
                                                                            If your article is about a stock(s), please consider including a chart provide context.
                                                                        </li>
                                                                        <li class="icon-minus" style="text-indent: -2.3em;">
                                                                            Hyperlink in-line to all source material. Keep anchorer text brief, and think carefully about which word you want to use as anchor text. Keyword hijacking is forbidden.
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="post-footer">
                                                                    &nbsp;
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <ul class="services-list">
                                                                <li class="marine-header6-serviceslist-li">
                                                                    <h4 class="marine-heade6-services-h3">Article Category :</h4>
                                                                </li>
                                                            </ul>
                                                            <div class="row" style="text-align: left !important;">
                                                                <div class="col-md-6" >
                                                                    <select class="js-example-basic-multiple" name="article_type" style="width: 100%; height: 40px;">
                                                                        <optgroup label="Market News">
                                                                            <option value="1A" @if($data->article_type == '1A') selected @endif>Indonesian Economy</option>
                                                                            <option value="1B" @if($data->article_type == '1B') selected @endif>International Economy</option>
                                                                            <option value="1C" @if($data->article_type == '1C') selected @endif>Company Announcement</option>
                                                                        </optgroup>
                                                                        <optgroup label="Market Outlook">
                                                                            <option value="2A" @if($data->article_type == '2A') selected @endif>News</option>
                                                                        </optgroup>
                                                                        <optgroup label="Stock Ideas">
                                                                            <option value="3A" @if($data->article_type == '3A') selected @endif>Buy Ideas</option>
                                                                            <option value="3B" @if($data->article_type == '3B') selected @endif>Sell Ideas</option>
                                                                            <option value="3C" @if($data->article_type == '3C') selected @endif>Sectors</option>
                                                                        </optgroup>
                                                                        <optgroup label="Broker">
                                                                            <option value="4A" @if($data->article_type == '4A') selected @endif>News</option>
                                                                        </optgroup>
                                                                        <optgroup label="Funds">
                                                                            <option value="5A" @if($data->article_type == '5A') selected @endif>Index Funds</option>
                                                                            <option value="5B" @if($data->article_type == '5B') selected @endif>Active Funds</option>
                                                                        </optgroup>
                                                                        <optgroup label="IR Presentation">
                                                                            <option value="6A" @if($data->article_type == '6A') selected @endif>News</option>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">&nbsp;</div>
                                                            </div>
                                                            <p>&nbsp;</p>
                                                            <ul class="services-list">
                                                                <li class="marine-header6-serviceslist-li">
                                                                    <h4 class="marine-heade6-services-h3">Other Tags :</h4>
                                                                </li>
                                                            </ul>
                                                            <div class="row" style="text-align: left !important;">
                                                                <div class="col-md-6" >
                                                                    <select class="js-example-basic-multiple" name="tag[]" multiple="multiple" style="width: 100%; height: 40px;">
                                                                        <option value="1" @if($data->tags_article->where('id_tag', 1)->toArray()) selected @endif>Alternative Investment</option>
                                                                        <option value="2" @if($data->tags_article->where('id_tag', 2)->toArray()) selected @endif>Bonds</option>
                                                                        <option value="3" @if($data->tags_article->where('id_tag', 3)->toArray()) selected @endif>Cash Invesment</option>
                                                                        <option value="4" @if($data->tags_article->where('id_tag', 4)->toArray()) selected @endif>Closed Funds</option>
                                                                        <option value="5" @if($data->tags_article->where('id_tag', 5)->toArray()) selected @endif>Commodities</option>
                                                                        <option value="6" @if($data->tags_article->where('id_tag', 6)->toArray()) selected @endif>Dividends Stock Ideas & Income</option>
                                                                        <option value="7" @if($data->tags_article->where('id_tag', 7)->toArray()) selected @endif>Energy Stocks</option>
                                                                        <option value="8" @if($data->tags_article->where('id_tag', 8)->toArray()) selected @endif>Forex</option>
                                                                        <option value="9" @if($data->tags_article->where('id_tag', 9)->toArray()) selected @endif>Mutual Funds</option>
                                                                        <option value="10" @if($data->tags_article->where('id_tag', 10)->toArray()) selected @endif>REITs</option>
                                                                        <option value="11" @if($data->tags_article->where('id_tag', 11)->toArray()) selected @endif>Retiremens Savings</option>
                                                                        <option value="12" @if($data->tags_article->where('id_tag', 12)->toArray()) selected @endif>Stock - Long</option>
                                                                        <option value="13" @if($data->tags_article->where('id_tag', 13)->toArray()) selected @endif>Stock - Short</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">&nbsp;</div>
                                                            </div>
                                                            <p>&nbsp;</p>
                                                            <ul class="services-list">
                                                                <li class="marine-header6-serviceslist-li">
                                                                    <h4 class="marine-heade6-services-h3">
                                                                        Upload supporting documents
                                                                        <br>
                                                                        <small>( Financial models, source materials, etc. )</small>
                                                                    </h4>

                                                                </li>
                                                            </ul>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="iconic-input">
                                                                        <input type="file" class="form-control" name="file" placeholder="Upload your document">
                                                                        <small>Acceptable format: xlx, xlsx, doc, docs, docx, pdf, ppt</small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">&nbsp;</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="f1-buttons">
                                                                        <button type="button" class="btn btn-previous">Previous</button>
                                                                        <button type="submit" class="btn btn-submit">Submit</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">&nbsp;</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>

    <script>
        var options = {
            image_previewText: "Image Preveiw doeit.org",
            filebrowserUploadUrl: "{{ route('article.upload.image') }}",
            extraPlugins: 'filebrowser',
        };

        // CKEDITOR.replace('#article', options);
        $('#article').ckeditor(options);
    </script>

    <script src="{{ asset('web/wizard_assets/js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/scripts.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
            $(".js-example-basic-multiple").select2();
        }); //ready
    </script>
@endsection
