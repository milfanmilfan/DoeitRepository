@extends('layouts.author.index_home')

@section('style')
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/css/style.css') }}">

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="container">
            <div class="row">
                <section class="main-content col-lg-12 col-md-12 col-sm-12 small-padding">
                    <div class="row">
                        <div id="post-items">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <ul class="services-list">
                                        <li class="marine-header6-serviceslist-li" style="text-align: left; margin-top: 20px;">
                                            <h3 class="marine-heade6-services-h3"><strong>Manage Your Article</strong></h3>
                                        </li>
                                        <li class="marine-header6-serviceslist-li">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: left !important;">
                                <section class="col-lg-9 col-md-8 col-sm-8 small-padding col-md-push-4 col-lg-push-3">
                                    <div class="masonry-container">
                                        @foreach ($data as $d)
                                        <div class="col-lg-6 col-md-6 col-sm-6 masonry-box" style="margin-bottom: 20px;">
                                            <div class="blog-post masonry">
                                                <div class="post">
                                                    <div class="post-content">
                                                        <ul class="post-meta">
                                                            <li>
                                                                <a href="javascript:void(0)">
                                                                    @if (($d->article_category ) === 1)
                                                                        Article Category : Can be Published
                                                                    @elseif (($d->article_category ) === 2)
                                                                        Article Category : Only this Website
                                                                    @else
                                                                        Article Category : Instablog
                                                                    @endif
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)">
                                                                    @if ((($d->article_type ) === "1A") || (($d->article_type ) === "1B") || (($d->article_type ) === "1C"))
                                                                        Article Type : Market News
                                                                    @elseif ((($d->article_type ) === "2A"))
                                                                        Article Type : Market Outlook
                                                                    @elseif ((($d->article_type ) === "3A") || (($d->article_type ) === "3B") || (($d->article_type ) === "3C"))
                                                                        Article Type : Stock Ideas
                                                                    @elseif ((($d->article_type ) === "4A"))
                                                                        Article Type : Broker
                                                                    @elseif ((($d->article_type ) === "5A") || (($d->article_type ) === "5B"))
                                                                        Article Type : Funds
                                                                    @elseif ((($d->article_type ) === "6A"))
                                                                        Article Type : IR Presentation
                                                                    @endif
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="post-details">
                                                            <h4 class="post-title">
                                                                <span class="post-format"></span>
                                                                <a href="{{ route('author.article.detail', $d->slug) }}" title="{{$d->title}}">{{$d->title}}</a>
                                                            </h4>
                                                        </div>
                                                        <p class="latest-from-blog_item_text">
                                                            {!!substr($d->description, 0, 200).' ...'!!}
                                                        </p>
                                                        <a class="read-more big" href="{{ route('author.article.detail', $d->slug) }}" title="{{$d->title}}">View Article</a>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </section>
                                @include('layouts.author.core.sidebar_menu_article')
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>

    {{--    <script src="{{ asset('web/wizard_assets/js/jquery-1.11.1.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('web/wizard_assets/bootstrap/js/bootstrap.min.js') }}"></script>--}}
    <script src="{{ asset('web/wizard_assets/js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/scripts.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
        }); //ready
    </script>
@endsection
