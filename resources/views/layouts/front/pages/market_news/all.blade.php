@extends('layouts.front.index')

@section('style')
    <link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}" type="image/x-icon"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/custom.css') }}' type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/table.css') }}" media="screen"/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="row contents">
            <div class="col-lg-3 col-md-4 col-sm-4 small-padding">
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4>Top News</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="calendar-2" class="widget widget_calendar">
                    <div id="categories-3" class="widget widget_categories">
                        @foreach ($top as $b)
                            <h6><a href="{{ route('article.detail', $b->slug) }}">{{ $b->title }}</a></h6><hr>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-4 small-padding">
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4>Market News</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="post blog-post blog-post-classic col-lg-12 col-md-12 col-sm-12">
                    @foreach ($marketnews as $b)
                        <div class="blog-post-list ">
                            <div class="blog-post-content">
                                <div class="post-content">
                                    <h4><a href="{{ route('article.detail', $b->slug) }}" title="{{$b->title}}">{{$b->title}}</a></h4>
                                    <div class="font-black">
                                      {!!substr($b->description, 0, 300).' ...'!!}
                                    </div>
                                    <ul class="post-meta">
                                        <li><a href="#">Author : {{$b->author->name}}</a></li>
                                        <li>{{$b->article_comment_count}} comments</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 small-padding">
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4>Market</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="calendar-2" class="widget widget_calendar">
                    <div id="categories-3" class="widget widget_categories">
                        <ul class="services-list">
                            <li class="marine-header6-serviceslist-li">
                                <div class="row">
                                    <div class="col-md-4">
                                        <strong>Dow</strong><br>
                                        <span>20,504.41</span>
                                    </div>
                                    <div class="col-md-4">
                                        <span>+0.45%</span><br>
                                        <span>+92.25</span>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{ asset('web/img/grafik-market-mini.png') }}" alt="">
                                    </div>
                                </div>
                            </li>
                            <li class="marine-header6-serviceslist-li">
                                <div class="row">
                                    <div class="col-md-4">
                                        <strong>S&P 500</strong><br>
                                        <span>20,504.41</span>
                                    </div>
                                    <div class="col-md-4">
                                        <span>+0.45%</span><br>
                                        <span>+92.25</span>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{ asset('web/img/grafik-market-mini.png') }}" alt="">
                                    </div>
                                </div>
                            </li>
                            <li class="marine-header6-serviceslist-li">
                                <div class="row">
                                    <div class="col-md-4">
                                        <strong>Dow</strong><br>
                                        <span>20,504.41</span>
                                    </div>
                                    <div class="col-md-4">
                                        <span>+0.45%</span><br>
                                        <span>+92.25</span>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{ asset('web/img/grafik-market-mini.png') }}" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4 style="text-align: left;">TOP GAINERS (NYSE, NASDAQ)</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="rwd-table">
                            <tr style="font-size: 10px;">
                                <th style="width:40%;">Stock</th>
                                <th>&nbsp</th>
                                <th>Today</th>
                                <th>3&nbsp;Month</th>
                                <th>1&nbsp;Year</th>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="">IMNP</td>
                                <td data-th="Today">34.7%</td>
                                <td data-th="3 Month">34.7%</td>
                                <td data-th="1 Year">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Today">34.7%</td>
                                <td data-th="3 Month">34.7%</td>
                                <td data-th="1 Year">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Today">34.7%</td>
                                <td data-th="3 Month">34.7%</td>
                                <td data-th="1 Year">34.7%</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4 style="text-align: left;">TOP LOOSERS (NYSE, NASDAQ)</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="rwd-table">
                            <tr style="font-size: 10px;">
                                <th style="width:40%;">Stock</th>
                                <th>&nbsp</th>
                                <th>Today</th>
                                <th>3&nbsp;Month</th>
                                <th>1&nbsp;Year</th>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="blog-post-list">
                    <div class="blog-post-content">
                        <div class="post-content">
                            <ul class="post-meta">
                                <li>
                                    <h4 style="text-align: left;">INA EQUITIES</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="rwd-table">
                            <tr style="font-size: 10px;">
                                <th style="width:40%;">Stock</th>
                                <th>&nbsp</th>
                                <th>Today</th>
                                <th>3&nbsp;Month</th>
                                <th>1&nbsp;Year</th>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                            <tr style="font-size: 10px;">
                                <td data-th="Name"><a class="link_" href="">Immune Pharmacy</a></td>
                                <td data-th="Symbol">IMNP</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                                <td data-th="Symbol">34.7%</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
        }); //ready
    </script>
@endsection
