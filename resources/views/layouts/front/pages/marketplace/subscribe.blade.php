@extends('layouts.front.index')

@section('style')
    <link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}" type="image/x-icon"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/custom.css') }}' type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/table.css') }}" media="screen"/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>
    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="row contents">
            <section class="main-content col-lg-12 col-md-12 col-sm-12 small-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row row-fluid section-header margin-bottom35">
                            <h1 class="">SUBSCRIBE PACKAGE</h1>
                            <h2 class="">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque..</h2>
                        </div>
                        <div class="row row-fluid padding-top20 padding-bottom60 margin-bottom35">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="image-box image-box-style2">
                                            <div class="text-wrap">
                                                <h4>Get your first experiences</h4>
                                                <br>
                                                <h3>STARTER</h3>
                                                <div class="callout-details-wrap">
                                                    <p class="font-black">
                                                        You got <strong>20 reports or articles</strong> in every category <br>
                                                        Only<strong> IDR 50.000/ Month</strong>
                                                    </p>
                                                    <p>
                                                        <a class="button blue" href="{{ url('marketplace/getpayment/starter') }}">SUBSCRIBE NOW! </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="image-box image-box-style2">
                                            <div class="text-wrap">
                                                <h4>Get more information for yourself</h4>
                                                <h3>INTERMEDIATE</h3>
                                                <div class="callout-details-wrap font-black">
                                                    <p class="font-black">
                                                        You got <strong>50 reports or articles</strong> in every category <br>
                                                        Only<strong> IDR 100.000/ Month</strong>
                                                    </p>
                                                    <p>
                                                        <a class="button blue" href="{{ url('marketplace/getpayment/intermediate') }}">SUBSCRIBE NOW! </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="image-box image-box-style2">
                                            <div class="text-wrap">
                                                <h4>Get your data from taking a risk</h4>
                                                <h3>ADVANCE</h3>
                                                <div class="callout-details-wrap font-black">
                                                    <p class="font-black">
                                                        You got <strong>unlimited</strong> reports or articles in every category <br>
                                                        Only<strong> IDR 200.000/ Month</strong>
                                                    </p>
                                                    <p>
                                                        <a class="button blue" href="{{ url('marketplace/getpayment/advance') }}">SUBSCRIBE NOW! </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="image-box image-box-style2">
                                            <div class="text-wrap">
                                                <h4>Get your data from exclusive contributor</h4>
                                                <h3>PRO</h3>
                                                <div class="callout-details-wrap font-black">
                                                    <p class="font-black">
                                                        This package is for article who has a document sharing from contributor
                                                        <br>
                                                        and you must paid base on every single article that you want.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4><a href="" title="" class="font-black">Your Subscription Include : </a></h4>
                                        <ul class="list check-style mr50">
                                            <li class="indents font-black">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi consequatur cum deleniti doloribus earum eligendi enim exercitationem expedita inventore ipsam laudantium natus nobis, odit reiciendis rem rerum, suscipit veritatis.
                                            </li>
                                            <li class="indents font-black">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi consequatur cum deleniti doloribus earum eligendi enim exercitationem expedita inventore ipsam laudantium natus nobis, odit reiciendis rem rerum, suscipit veritatis.
                                            </li>
                                            <li class="indents font-black">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi consequatur cum deleniti doloribus earum eligendi enim exercitationem expedita inventore ipsam laudantium natus nobis, odit reiciendis rem rerum, suscipit veritatis.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
        }); //ready
    </script>
@endsection