<div id="login" class="modalDialog">
    <div>
        <a href="#close" title="Close" class="close">X</a>

        <h2>Login</h2>
        <form class="get-in-touch light align-left" method="post" action="{{url('authors/login')}}">
        {{csrf_field()}}
            <div class="iconic-input">
                <input type="text" name="email" placeholder="Your Email">
                <i class="icons icon-mail"></i>
            </div>
            <div class="iconic-input">
                <input type="password" name="password" placeholder="Password">
                <i class="icons icon-user-1"></i>
            </div>
            <input type="submit" value="Send">

        </form>
        <span style="color: #000000">Don't Have any Account ?</span> <a href="" style="color: ##008fd5"> Register here</a>
        <div class="msg">
        </div>
    </div>
</div>