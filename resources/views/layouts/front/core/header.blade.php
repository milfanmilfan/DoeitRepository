<header id="header" class="style7">
    <div id="upper-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="item left hidden-separator">
                        <ul id="menu-shop-header" class="menu">

                            <li class="menu-item"><a href="#">&nbsp;</a></li>
                            <li class="menu-item"><a href="#">&nbsp;</a></li>
                        </ul>
                    </div>
                    <div class="item right hidden-separator">
                        <div class="cart-menu-item ">
                            @if(auth()->check())
                                <a href="@if(auth()->user()->author == true) {{ route('author.home') }} @else {{ route('member.home') }} @endif"><strong>Hello</strong>, {{ auth()->user()->name }}</a> |
                                <a href="{{ route('user.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Sign Out
                                </a>
                                <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            @else
                                <a href="{{ url('account/sign-in') }}"><strong>Sign In | </strong></a> 
                                <a href="{{ url('member/mail/register') }}"><strong>Register</strong></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 logo">
                    <a href="{{ url('/') }}" title="Doeit" rel="home"><img class="logo" src="{{ asset('web/img/logo.png') }}" alt="Doeit"></a>
                    <div id="main-nav-button">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="col-md-10">
                    <div id="search-box" class="align-right">
                        <i class="icons icon-search"></i>
                        <form role="search" method="get" id="searchform" action="#">
                            <input type="text" name="s" placeholder="Search here..">
                            <div class="iconic-submit">
                                <div class="icon">
                                    <i class="icons icon-search"></i>
                                </div>
                                <input type="submit" value="">
                            </div>
                        </form>
                    </div>
                    @include('layouts.front.core.navbar')
                </div>
            </div>
        </div>
    </div>
</header>