<fieldset>
    <div class="row">
        <div class="col-md-12">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3"><strong>Get Free Newsletter delivered to your inbox</strong></h3>
                    <span style="font-size: 14pt;"><strong>Doeit</strong> publishes top investment analysis on hundreds of topics, Based on your selected interest. Your might like :</span><br>
                </li>
                <li class="marine-header6-serviceslist-li">
                    &nbsp;
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <div class="iconic-input">
                        <input type="radio" value="1A" name="news" @if(is_array(old('news')) && in_array(1, old('news'))) checked @endif />
                        <span style="font-size: 16px;"><strong>Macro View</strong></span>
                        <p>
                            Outlook for the overall market and ots key drivers <br>
                            with sections on the Economy, the Dollar, Commodities, and Real Estate
                        </p>
                    </div>
                    <div class="iconic-input">
                        <input type="radio" value="2A" name="news" @if(is_array(old('news')) && in_array(2, old('news'))) checked @endif >
                        <span style="font-size: 16px;"><strong>Morning Briefing</strong></span>
                        <p>
                            The top article from <strong>Doeit</strong> homepage <br>
                            plus the most popular and most commented article on the site today
                        </p>
                    </div>
                    <div class="iconic-input">
                        <input type="radio" value="3A" name="news" @if(is_array(old('news')) && in_array(3, old('news'))) checked @endif>
                        <span style="font-size: 16px;"><strong>Wall Street Breakfast</strong></span>
                        <p>
                            One-page summary oh the day's top financial and business stories <br>
                            popular with portfolio managers and executive
                        </p>
                    </div>
                    <div class="iconic-input">
                        <input type="radio" value="4A" name="news" @if(is_array(old('news')) && in_array(4, old('news'))) checked @endif>
                        <span style="font-size: 16px;"><strong>FA Daily</strong></span>
                        <p>
                            Curated newsletter geared to the FA and RIA community with a focus <br>
                            on wealth management, retirement advice, building your practice and industry news. <br>
                            Published on all market days
                        </p>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    * Your may modify your choices or unsubscribe at any time
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="f1-buttons pull-right">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
            </div>
        </div>
    </div>
</fieldset>