<fieldset>
    <div class="row">
        <div class="col-md-6">
            <div class="pdleft70">
                <h3 class="marine-heade6-services-h3"><strong>Which stock do you follow ?</strong></h3>
                <span style="font-size: 14pt;">Customize Doeit to your interest.</span><br>

                <select class="js-example-basic-multiple" multiple="multiple" name="stocks[]" style="width: 100%;">
                    <option value="A1">Select 1</option>
                    <option value="A2">Select 2</option>
                    <option value="A3">Select 3</option>
                    <option value="A4">Select 4</option>
                </select>
                <p>&nbsp;</p>
                <div class="iconic-input">
                    <input type="checkbox" value="1" name="alerts" @if(is_array(old('alerts')) && in_array(1, old('alerts'))) checked @endif />
                    <span>Send me alerts when news and articles are published on my stocks</span>
                </div>
                <p>&nbsp;</p>
                <p>
                    *The stock you tracking won't be shown pulicly. In no way should the appearance of securities on this page be interpreted as a recomendation to buy.
                </p>
            </div>
        </div>
        <div class="col-md-6">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">
                <hr>
            </div>
            <div class="col-md-6">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Previous</button>
                    <button type="submit" class="btn btn-submit">Submit</button>
                </div>
            </div>
            <div class="col-md-6">&nbsp;</div>
        </div>
    </div>
</fieldset>