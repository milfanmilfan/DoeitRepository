<fieldset>
    <div class="row">
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Market News</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Indonesian Economy</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="1B" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>International Economy</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="1C" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Company Announcement</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Market Outlook</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="2A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Market Outlook News</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Stock Ideas</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="3A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Buy Ideas</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3B" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Sell Ideas</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3C" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Sectors</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Broker Report</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="4A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Broker Report News</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="5A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Index Funds</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5B" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>Active Funds</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">IR Presentation</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="6A" name="newscategory[]" @if(is_array(old('newscategory')) && in_array(1, old('newscategory'))) checked @endif />
                        <span>IR Presentation News</span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Financial Advisor</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1A" name="category[]" @if(is_array(old('category')) && in_array(1, old('category'))) checked @endif />
                        <span>Registered Investing Advisor (RIA)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2A" name="category[]" @if(is_array(old('category')) && in_array(2, old('category'))) checked @endif >
                        <span>Certified Financial Planner (CFP)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3A" name="category[]" @if(is_array(old('category')) && in_array(3, old('category'))) checked @endif>
                        <span>Licensed Securities Brokers</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4A" name="category[]" @if(is_array(old('category')) && in_array(4, old('category'))) checked @endif>
                        <span>Insurance Brokers</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5A" name="category[]" @if(is_array(old('category')) && in_array(5, old('category'))) checked @endif>
                        <span>Accountant</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Hedge Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1B" name="category[]" @if(is_array(old('category')) && in_array(6, old('category'))) checked @endif/>
                        <span>Hedge Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2B" name="category[]" @if(is_array(old('category')) && in_array(7, old('category'))) checked @endif>
                        <span>Hedge Fund Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3B" name="category[]" @if(is_array(old('category')) && in_array(8, old('category'))) checked @endif>
                        <span>Hedge Fund Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4B" name="category[]" @if(is_array(old('category')) && in_array(9, old('category'))) checked @endif>
                        <span>Hedge Fund Director of Research</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5B" name="category[]" @if(is_array(old('category')) && in_array(10, old('category'))) checked @endif>
                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Mutual Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1C" name="category[]" @if(is_array(old('category')) && in_array(11, old('category'))) checked @endif />
                        <span>Mutual Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2C" name="category[]" @if(is_array(old('category')) && in_array(12, old('category'))) checked @endif >
                        <span>Mutual Fund Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3C" name="category[]" @if(is_array(old('category')) && in_array(13, old('category'))) checked @endif>
                        <span>Mutual Fund Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4C" name="category[]" @if(is_array(old('category')) && in_array(14, old('category'))) checked @endif>
                        <span>Mutual Fund Sales</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5C" name="category[]" @if(is_array(old('category')) && in_array(15, old('category'))) checked @endif>
                        <span>Hedge Fund Executive (CFO/COO/CEO)</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Pension Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1D" name="category[]" @if(is_array(old('category')) && in_array(16, old('category'))) checked @endif />
                        <span>Pension Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2D" name="category[]" @if(is_array(old('category')) && in_array(17, old('category'))) checked @endif >
                        <span>Pension Fund Analyst</span>
                    </div>
                </li>

            </ul>
        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Endowment Funds</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1E" name="category[]" @if(is_array(old('category')) && in_array(18, old('category'))) checked @endif />
                        <span>Endowment Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2E" name="category[]" @if(is_array(old('category')) && in_array(19, old('category'))) checked @endif >
                        <span>Endowment Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Prive Equiy</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1F" name="category[]" @if(is_array(old('category')) && in_array(20, old('category'))) checked @endif />
                        <span>Prive Equity Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2F" name="category[]" @if(is_array(old('category')) && in_array(21, old('category'))) checked @endif >
                        <span>Prive Equity Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Venture Capital</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1G" name="category[]" @if(is_array(old('category')) && in_array(22, old('category'))) checked @endif />
                        <span>Venture Capital Fund Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2G" name="category[]" @if(is_array(old('category')) && in_array(23, old('category'))) checked @endif >
                        <span>Venture Capital Fund Analyst</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Company Executive</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1H" name="category[]" @if(is_array(old('category')) && in_array(24, old('category'))) checked @endif />
                        <span>CEO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2H" name="category[]" @if(is_array(old('category')) && in_array(25, old('category'))) checked @endif >
                        <span>CFO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3H" name="category[]" @if(is_array(old('category')) && in_array(26, old('category'))) checked @endif >
                        <span>COO</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4H" name="category[]" @if(is_array(old('category')) && in_array(27, old('category'))) checked @endif >
                        <span>Board Member</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5H" name="category[]" @if(is_array(old('category')) && in_array(28, old('category'))) checked @endif >
                        <span>Small Bussiness Owner</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6H" name="category[]" @if(is_array(old('category')) && in_array(29, old('category'))) checked @endif >
                        <span>Other C-Level Excetive</span>
                    </div>
                </li>
            </ul>

        </div>
        <div class="col-md-4">
            <ul class="services-list">
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Other Financials</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1I" name="category[]" @if(is_array(old('category')) && in_array(30, old('category'))) checked @endif />
                        <span>Investment Consultan</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2I" name="category[]" @if(is_array(old('category')) && in_array(31, old('category'))) checked @endif >
                        <span>Investment Banker/Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3I" name="category[]" @if(is_array(old('category')) && in_array(32, old('category'))) checked @endif >
                        <span>Family Office Manager/Analyst</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4I" name="category[]" @if(is_array(old('category')) && in_array(33, old('category'))) checked @endif >
                        <span>Sell Side Research Analyst/Trader</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5I" name="category[]" @if(is_array(old('category')) && in_array(34, old('category'))) checked @endif >
                        <span>Commecial Banker</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6I" name="category[]" @if(is_array(old('category')) && in_array(35, old('category'))) checked @endif >
                        <span>Operation Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7I" name="category[]" @if(is_array(old('category')) && in_array(36, old('category'))) checked @endif >
                        <span>Complience Manager</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="8I" name="category[]" @if(is_array(old('category')) && in_array(37, old('category'))) checked @endif >
                        <span>IR Professional</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Individual Investor</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1J" name="category[]" @if(is_array(old('category')) && in_array(38, old('category'))) checked @endif />
                        <span>Full-time Investor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2J" name="category[]" @if(is_array(old('category')) && in_array(39, old('category'))) checked @endif >
                        <span>Occasional Investor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3J" name="category[]" @if(is_array(old('category')) && in_array(40, old('category'))) checked @endif >
                        <span>Retiree</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Academic / Student (Current)</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1K" name="category[]" @if(is_array(old('category')) && in_array(41, old('category'))) checked @endif />
                        <span>High School Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2K" name="category[]" @if(is_array(old('category')) && in_array(42, old('category'))) checked @endif >
                        <span>Undergraduated Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3K" name="category[]" @if(is_array(old('category')) && in_array(43, old('category'))) checked @endif >
                        <span>Chartered Financial Analyst (CFA)</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4K" name="category[]" @if(is_array(old('category')) && in_array(44, old('category'))) checked @endif >
                        <span>MBA Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5K" name="category[]" @if(is_array(old('category')) && in_array(45, old('category'))) checked @endif >
                        <span>Other Graduate Program</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6K" name="category[]" @if(is_array(old('category')) && in_array(46, old('category'))) checked @endif >
                        <span>Doctoral Student</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7K" name="category[]" @if(is_array(old('category')) && in_array(47, old('category'))) checked @endif >
                        <span>Profesor</span>
                    </div>
                </li>
                <li class="marine-header6-serviceslist-li">
                    <h3 class="marine-heade6-services-h3">Other Non-Financial Professional</h3>
                    <div class="iconic-input">
                        <input type="checkbox" value="1L" name="category[]" @if(is_array(old('category')) && in_array(48, old('category'))) checked @endif />
                        <span>PR Professional</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="2L" name="category[]" @if(is_array(old('category')) && in_array(49, old('category'))) checked @endif >
                        <span>Lawyer</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="3L" name="category[]" @if(is_array(old('category')) && in_array(50, old('category'))) checked @endif >
                        <span>Doctor</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="4L" name="category[]" @if(is_array(old('category')) && in_array(51, old('category'))) checked @endif >
                        <span>Writer</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="5L" name="category[]" @if(is_array(old('category')) && in_array(52, old('category'))) checked @endif >
                        <span>Journalist</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="6L" name="category[]" @if(is_array(old('category')) && in_array(53, old('category'))) checked @endif >
                        <span>Professional Blogger</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="7L" name="category[]" @if(is_array(old('category')) && in_array(54, old('category'))) checked @endif >
                        <span>Newsletter Author</span>
                    </div>
                    <div class="iconic-input">
                        <input type="checkbox" value="8L" name="category[]" @if(is_array(old('category')) && in_array(55, old('category'))) checked @endif >
                        <span>Other</span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <ul class="services-list">
                        <li class="marine-header6-serviceslist-li">
                            <h3 class="marine-heade6-services-h3">Let us know you</h3>
                            <div class="iconic-input">
                                <input type="text" name="first_name" placeholder="First Name*">
                            </div>
                            <div class="iconic-input">
                                <input type="text" name="last_name" placeholder="Last Name*">
                            </div>
                        </li>
                        <li class="marine-header6-serviceslist-li">
                            <h3 class="marine-heade6-services-h3">Are you Accredited Investor ?</h3>
                            <div class="iconic-input">
                                <input type="radio" id="ya" value="1" name="accredited" class="yescredit">
                                <span>Yes</span>
                            </div>
                            <div class="iconic-input">
                                <input type="radio" id="no" value="0" name="accredited" class="nocredit">
                                <span>No</span>
                            </div>
                            <div class="credits" style="display: none;">
                                <h3 class="marine-heade6-services-h3">What is the current value of your assets under management (AUM ?)</h3>
                                <select class="js-example-basic-single" name="valueaum" style="width: 100%; height: 40px;">
                                    <option value="1">0 - 1M</option>
                                    <option value="2">1M - 100M</option>
                                    <option value="3">100M -500M</option>
                                </select>
                            </div>
                        </li>
                        <li class="marine-header6-serviceslist-li">

                        </li>
                    </ul>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="services-list">
                                <li class="marine-header6-serviceslist-li">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="marine-heade6-services-h3">Select Your Broker(s)</h3>
                                        </div>
                                        <div class="col-md-4">
                                            <h3 class="marine-heade6-services-h3">Primary</h3>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="services-list">
                                <li class="marine-header6-serviceslist-li">
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1A" name="has_broker" class="brok">
                                                <span>I don't have a brokers</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1B" name="broker[]" class="brok1">
                                                <span>Bloomberg Terminal</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1B" name="primarybrok[]" class="primarybrok1">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1C" name="broker[]" class="brok2">
                                                <span>Capital IQ</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1C" name="primarybrok[]" class="primarybrok2">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1D" name="broker[]" class="brok3">
                                                <span>FactSet</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1D" name="primarybrok[]" class="primarybrok3">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1E" name="broker[]" class="brok4">
                                                <span>Thompson One</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1E" name="primarybrok[]" class="primarybrok4">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1F" name="broker[]" class="brok5">
                                                <span>Thompson Reutes One</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1F" name="primarybrok[]" class="primarybrok5">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <br>
                                    <p class="marine-heade6-services-h3">Search Your Broker Name </p>
                                    <select class="js-example-basic-multiple brokname" multiple="multiple" name="broker_name[]" style="width: 100%;">
                                        <option value="A1">Select 1</option>
                                        <option value="A2">Select 2</option>
                                        <option value="A3">Select 3</option>
                                        <option value="A4">Select 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="services-list">
                                <li class="marine-header6-serviceslist-li">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="marine-heade6-services-h3">Select The Professional
                                                Platform(s) you use</h3>
                                        </div>
                                        <div class="col-md-4">
                                            <h3 class="marine-heade6-services-h3">Primary</h3>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="services-list">
                                <li class="marine-header6-serviceslist-li">
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1N" name="has_platform" class="platformbrok">
                                                <span>I don't have a professional Platform</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1O" name="platforms[]" class="platformbrok1">
                                                <span>Bloomberg Terminal</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1O" name="platformbrok[]" class="platformprimarybrok1">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1P" name="platforms[]" class="platformbrok2">
                                                <span>Capital IQ</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1P" name="platformbrok[]" class="platformprimarybrok2">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1Q" name="platforms[]" class="platformbrok3">
                                                <span>FactSet</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1Q" name="platformbrok[]" class="platformprimarybrok3">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1R" name="platforms[]" class="platformbrok4">
                                                <span>Thompson One</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1R" name="platformbrok[]" class="platformprimarybrok4">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="border-bottom: 1px #73848e solid">
                                        <div class="col-md-8">
                                            <div class="iconic-input">
                                                <input type="checkbox" value="1T" name="platforms[]" class="platformbrok5">
                                                <span>Thompson Reutes One</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="iconic-input">
                                                <input type="radio" value="1T" name="platformbrok[]" class="platformprimarybrok5">
                                                <span>Yes</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <br>
                                    <p class="marine-heade6-services-h3">Search Your Broker Name </p>
                                    <select class="js-example-basic-multiple platformname" multiple="multiple" name="profesional_platform[]" style="width: 100%;">
                                        <option value="A1">Select 1</option>
                                        <option value="A2">Select 2</option>
                                        <option value="A3">Select 3</option>
                                        <option value="A4">Select 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <div class="f1-buttons pull-right">
                <button type="button" class="btn btn-next" style="width: 100px;">Next</button>
            </div>
        </div>
    </div>
</fieldset>