@extends('layouts.member.index_home')

@section('style')
    <link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}" type="image/x-icon"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/custom.css') }}' type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/table.css') }}" media="screen"/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");

    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="container">
            <div class="row">
                <div class="page-heading style3 wrapper border-bottom ">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h1>{{$data->title}}</h1>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <section class="main-content col-lg-9 col-md-8 col-sm-8 small-padding">
                    <div class="row">
                        <div id="post-items">
                            <div class="post blog-post blog-post-classic col-lg-12 col-md-12 col-sm-12 margin-top20">
                                <div class="blog-post-list">
                                    <div class="blog-post-meta">
                                        <span class="post-date">
                                            <span class="post-day">{{ $data->created_at->format('d') }}</span>
                                                <br/>
                                            {{ $data->created_at->format('M, Y') }}
                                        </span>
                                        <p>&nbsp;</p>
                                        <span class="post-format">
                                            <img src="{{ asset('web/img/profile') }}/{{ $user->profile_pictures }}" alt="" class="profile_art">
                                        </span>
                                        <a href="{{ url('authors/profile') }}/{{ $data->id_author }}"><span class="author">by {{ $data->author->name }}</span></a>
                                    </div>
                                    <div class="blog-post-content">
                                        <div class="post-content font-black">
                                            {!! $data->article !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @if ($status == 1)
                                <div id="comments" class="post-comments">
                                    <div id="respond" class="comment-respond">
                                        <h3 id="reply-title" class="comment-reply-title">Leave A Reply Form <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small></h3>
                                        <form method="post" id="commentform" class="comment-form">
                                            {{csrf_field()}}
                                            <input type="hidden" name="id_article" value="{{$data->id}}">

                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="iconic-input">
                                                        <input placeholder="Name*" id="author" name="name" type="text" value="" required="" size="30">
                                                        <i class="icons icon-user-1"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="iconic-input">
                                                        <input id="email" placeholder="E-mail*" name="email" type="text" value="" required="" size="30">
                                                        <i class="icons icon-mail-4"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <textarea placeholder="Message" required="" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
                                                </div>
                                                <input type="submit" value="Send Message">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {


            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
        }); //ready
    </script>
@endsection
