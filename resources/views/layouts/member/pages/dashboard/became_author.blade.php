@extends('layouts.member.index_home')

@section('style')
    <link rel="shortcut icon" href="{{ asset('web/img/logo.png') }}" type="image/x-icon"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' id='twitter-bootstrap-css' href='{{ asset('web/css/bootstrap.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css' href='{{ asset('web/css/fontello.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyphoto-css-css' href='{{ asset('web/js/prettyphoto/css/prettyPhoto.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css' href='{{ asset('web/css/animation.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css' href='{{ asset('web/css/flexslider.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css' href='{{ asset('web/css/perfect-scrollbar-0.4.10.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css' href='{{ asset('web/css/jquery.validity.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{ asset('web/css/jquery-ui.min.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{ asset('web/css/style.css') }}' type='text/css' media='all'/>
    <link rel='stylesheet' id='mobilenav-css' href='{{ asset('web/css/mobilenav.css') }}' type='text/css' media="screen and (max-width: 838px)"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.revslider.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('web/js/rs-plugin/css/settings.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('web/wizard_assets/css/bootstrap.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('web/js/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('web/css/custom.css') }}" rel="stylesheet">

    <!-- jQuery -->
    <script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>
    <!-- Google Maps -->
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
    <!--[if lt IE 9]>
    <script>
        document.createElement("header");
        document.createElement("nav");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("footer");
        document.createElement("hgroup");
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="{{ asset('web/js/icomoon.js') }}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{ asset('css/ie.css') }}" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('js/script_ie.js') }}"></script>
    <![endif]-->
@endsection

@section('content')
    <section id="main-content">
        <div class="container">
            <div class="row">
                <section class="main-content col-lg-12 col-md-12 col-sm-12 small-padding">
                    <div class="row">
                        <div id="post-items">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <ul class="services-list">
                                        <li class="marine-header6-serviceslist-li" style="text-align: left; margin-top: 20px;">
                                            <h3 class="marine-heade6-services-h3"><strong>Registration as Author</strong></h3>
                                        </li>
                                        <li class="marine-header6-serviceslist-li">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <form role="form" method="post" id="regisform" class="f1 get-in-touch light" enctype="multipart/form-data" autocomplete="off">
                                    {{csrf_field()}}
                                    <div class="f1-steps" style="margin-bottom: 20px;">
                                        <div class="f1-progress">
                                            <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                                        </div>
                                        <div class="f1-step active">
                                            <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                                            <p>Personal Details</p>
                                        </div>
                                        <div class="f1-step">
                                            <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                                            <p>Your Article Profile</p>
                                        </div>
                                        <div class="f1-step">
                                            <div class="f1-step-icon"><i class="fa fa-twitter"></i></div>
                                            <p>Your Author Page</p>
                                        </div>
                                    </div>
                                    @include('layouts.author.pages.registration.field_personal_details')

                                    @include('layouts.author.pages.registration.field_articles')

                                    @include('layouts.author.pages.registration.field_authors')
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('web/js/bootstrap.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery-ui.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/SmoothScroll.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/prettyphoto/js/jquery.prettyPhoto.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/modernizr.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/wow.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.sharre.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.flexslider-min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.knob.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.mixitup.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.masonry.min.js?ver=3.1.2') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.fitvids.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.nouislider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/jquery.validity.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/tweetie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.enablelog.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('web/js/rs-plugin/js/jquery.themepunch.tools.min.js') }}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="{{ asset('web/wizard_assets/js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('web/wizard_assets/js/scripts.js') }}"></script>

    <script src="{{ asset('web/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web/js/custom.js') }}"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd MM yyyy'
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(e){
            @if ($errors->count() > 0)
                @foreach ($errors->getMessages() as $key => $message)

                    validate_script("{{ $key }}", "{{ $message[0] }}")

                    @endforeach
                    @endif

            var file = document.getElementById('profile_pict');
            function showPreview()
            {
                for(var i = 0; i < file.files.length; i++){
                    var each_file = file.files[i];
                    var reader = new FileReader();

                    reader.onload = function(e){
                        //$('.show-images').append('<img src="'+e.target.result+'" />');
                        $("#div-image").html('<img src="'+e.target.result+'" class="thumb-img img-thumbnail" style="max-width: 250px; height: auto;">');
                    }

                    reader.readAsDataURL(each_file);
                }
            }
            $("body").delegate('#profile_pict', 'change', function(e){
                showPreview();
            });

        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:16000,
                        startwidth:1170,
                        startheight:700,
                        hideThumbs:200,
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:5,
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",
                        touchenabled:"on",
                        onHoverStop:"on",
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                        keyboardNavigation:"off",
                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,
                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,
                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",
                        spinner:"spinner4",
                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,
                        shuffle:"off",
                        autoHeight:"off",
                        forceFullWidth:"off",
                        hideThumbsOnMobile:"off",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"off",
                        hideArrowsOnMobile:"off",
                        hideThumbsUnderResolution:0,
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        videoJsPath:"rs-plugin/videojs/",
                        fullScreenOffsetContainer: ""
                    });
            $('.primarybrok1').prop("disabled", true)
            $('.primarybrok2').prop("disabled", true)
            $('.primarybrok3').prop("disabled", true)
            $('.primarybrok4').prop("disabled", true)
            $('.primarybrok5').prop("disabled", true)

            $('.platformprimarybrok1').prop("disabled", true)
            $('.platformprimarybrok2').prop("disabled", true)
            $('.platformprimarybrok3').prop("disabled", true)
            $('.platformprimarybrok4').prop("disabled", true)
            $('.platformprimarybrok5').prop("disabled", true)

            $('.brok1').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.primarybrok1').prop("disabled", false)
                else $('.primarybrok1').prop("disabled", true)
            })
            $('.brok2').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.primarybrok2').prop("disabled", false)
                else $('.primarybrok2').prop("disabled", true)
            })
            $('.brok3').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.primarybrok3').prop("disabled", false)
                else $('.primarybrok3').prop("disabled", true)
            })
            $('.brok4').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.primarybrok4').prop("disabled", false)
                else $('.primarybrok4').prop("disabled", true)
            })
            $('.brok5').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.primarybrok5').prop("disabled", false)
                else $('.primarybrok5').prop("disabled", true)
            })

            $('.platformbrok1').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.platformprimarybrok1').prop("disabled", false)
                else $('.platformprimarybrok1').prop("disabled", true)
            })
            $('.platformbrok2').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.platformprimarybrok2').prop("disabled", false)
                else $('.platformprimarybrok2').prop("disabled", true)
            })
            $('.platformbrok3').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.platformprimarybrok3').prop("disabled", false)
                else $('.platformprimarybrok3').prop("disabled", true)
            })
            $('.platformbrok4').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.platformprimarybrok4').prop("disabled", false)
                else $('.platformprimarybrok4').prop("disabled", true)
            })
            $('.platformbrok5').on('change', function() {
                if ($(this).is(":checked") == true)
                    $('.platformprimarybrok5').prop("disabled", false)
                else $('.platformprimarybrok5').prop("disabled", true)
            })

            $('.brok').on('change', function() {
                if ($(this).is(":checked") == true)
                {
                    $('.brok1').prop("disabled", true)
                    $('.brok2').prop("disabled", true)
                    $('.brok3').prop("disabled", true)
                    $('.brok4').prop("disabled", true)
                    $('.brok5').prop("disabled", true)
                    $('.brokname').prop("disabled", true)
                }
                else
                {
                    $('.brok1').prop("disabled", false)
                    $('.brok2').prop("disabled", false)
                    $('.brok3').prop("disabled", false)
                    $('.brok4').prop("disabled", false)
                    $('.brok5').prop("disabled", false)
                    $('.brokname').prop("disabled", false)
                }
            })

            $('.platformbrok').on('change', function() {
                if ($(this).is(":checked") == true)
                {
                    $('.platformbrok1').prop("disabled", true)
                    $('.platformbrok2').prop("disabled", true)
                    $('.platformbrok3').prop("disabled", true)
                    $('.platformbrok4').prop("disabled", true)
                    $('.platformbrok5').prop("disabled", true)
                    $('.platformname').prop("disabled", true)
                }
                else
                {$('.platformbrok1').prop("disabled", false)
                    $('.platformbrok2').prop("disabled", false)
                    $('.platformbrok3').prop("disabled", false)
                    $('.platformbrok4').prop("disabled", false)
                    $('.platformbrok5').prop("disabled", false)
                    $('.platformname').prop("disabled", false)
                }
            })


            $(".js-example-basic-multiple").select2();
            $('.yescredit').click(function() {
                $('.credits').show()
            });
            $('.nocredit').click(function() {
                $('.credits').hide();
            });

        }); //ready
    </script>
@endsection