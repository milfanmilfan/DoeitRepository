<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    @include('layouts.member.core.head')
    @yield('style')
</head>
<body class="w1170 headerstyle2 preheader-on">
<div id="marine-content-wrapper">
    @include('layouts.member.core.header')

    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(session($msg))
            <br>
            <div class="col-md-6 col-md-offset-3 alert alert-{{ $msg }}" role="alert">{{ session($msg) }}</div>
        @endif
    @endforeach

    @if(count($errors->all()) > 0)
        <br>
        <div class="col-md-6 col-md-offset-3 alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                {{ $error }} <br>
            @endforeach
        </div>
    @endif

    <div id="marine-content-inner">
        @yield('content')
    </div>
    @include('layouts.member.core.footer')
</div>
@yield('script')
</body>
</html>