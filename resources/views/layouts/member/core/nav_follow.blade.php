<div class="container">
    <div class="page-heading style3 wrapper border-bottom ">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <ul class="nav-profile">
                    <li>
                        <a href="{{ url('account/profile/comments') }}">
                            <h4 class="{{ (Request::is('account/profile/comments') ? 'current' : Request::is('account/profile') ? 'current' : '') }}">
                                100
                                <br>
                                Comments
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('account/profile/stocktalks') }}">
                            <h4 class="{{ (Request::is('account/profile/stocktalks') ? 'current' : '') }}">
                                200
                                <br>
                                Stock Talks
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('account/profile/followers') }}">
                            <h4 class="{{ (Request::is('account/profile/followers') ? 'current' : '') }}">
                                100
                                <br>
                                Followers
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('account/profile/following') }}">
                            <h4 class="{{ (Request::is('account/profile/following') ? 'current' : '') }}">
                                100
                                <br>
                                Following
                            </h4>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p class="breadcrumbs">
                    &nbsp;
                </p>
            </div>
        </div>
    </div>
</div>