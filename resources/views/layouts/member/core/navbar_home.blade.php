<ul id="main-nav" class="menu">
    <li class="menu-item"><a href="{{ url('account/feed') }}">My Feed</a></li>
    <li class="menu-item"><a href="{{ url('account/profile') }}">My Profile</a></li>
    <li class="menu-item"><a href="{{ url('account/latest') }}">Latest</a></li>
    <li class="menu-item"><a href="{{ url('account/articles') }}">Articles</a></li>
    <li class="menu-item"><a href="{{ url('account/news') }}">News</a></li>
    <li class="menu-item"><a href="{{ url('account/data') }}">Data</a></li>
    <li class="menu-item"><a href="{{ url('account/alert') }}">Alerts</a></li>
    <li class="menu-item"><a href="{{ url('account/stock-talks') }}">Stock Talks</a></li>
    @if($already_paid > 0)
        <li class="menu-item"><a href="{{ url('account/paid-articles') }}">Paid Articles</a></li>
    @endif
    @if(auth()->user()->author == 0)
        <li class="menu-item"><a href="{{ url('account/became-author') }}">Became Author</a></li>
    @elseif(auth()->user()->author == 1)
        <li class="menu-item"><a href="{{ url('account/manage-article') }}">Manage Article</a></li>
    @endif
</ul>