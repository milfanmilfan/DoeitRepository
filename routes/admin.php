<?php

/*
|--------------------------------------------------------------------------
| Web Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', 'HomeController@login')->name('admin.login');
Route::get('admin/dashboard/', 'HomeController@index')->name('admin.dashboard');
Route::get('admin/article-request/', 'ArticleRequestController@index')->name('admin.article_request');
Route::get('admin/article-request/edit/{id}', 'ArticleRequestController@edit')->name('admin.article_request.edit');
Route::post('admin/article-request/{id}', 'ArticleRequestController@update')->name('admin.article_request.update');

Route::get('admin/published-article/', 'ManageArticleController@published')->name('admin.published_article');
Route::get('admin/rejected-article/', 'ManageArticleController@rejected')->name('admin.rejected_article');
Route::get('admin/suspends-article/', 'ManageArticleController@suspend')->name('admin.suspend_article');

Route::get('admin/member/', 'ListDataController@member')->name('admin.member');

Route::get('admin/intern-author/', 'ListDataController@intern_author')->name('admin.intern_author');
Route::get('admin/intern-author/add', 'InternAuthorController@add')->name('admin.intern_author_add');
Route::post('admin/intern-author', 'InternAuthorController@store')->name('admin.intern_author.store');

Route::get('admin/extern-author/', 'ListDataController@extern_author')->name('admin.extern_author');

Route::get('admin/author-request', 'AuthorRequestController@index')->name('admin.author_request');