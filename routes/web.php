<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

	Auth::routes();

//	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/', 'Front\HomeController@index');

	Route::get('market-news/all', 'Front\MarketNewsController@index');
	Route::get('market-news/indonesian-economy', 'Front\MarketNewsController@indonesian_economy');
	Route::get('market-news/international-economy', 'Front\MarketNewsController@international_economy');
	Route::get('market-news/company-announcement', 'Front\MarketNewsController@company_announcement');

	Route::get('market-outlook', 'Front\MarketOutlookController@index');

	Route::get('stock-ideas/buy-ideas', 'Front\StockIdeasController@buy_ideas');
	Route::get('stock-ideas/sell-ideas', 'Front\StockIdeasController@sell_ideas');
	Route::get('stock-ideas/sectors', 'Front\StockIdeasController@sectors');

	Route::get('discount/regional', 'Front\DiscountController@regional');
	Route::get('discount/newest', 'Front\DiscountController@newest');
	Route::get('discount/likes', 'Front\DiscountController@likes');

	Route::get('IR-presentation/latest', 'Front\IRPresentationController@latest');
	Route::get('IR-presentation/search-within-ir', 'Front\IRPresentationController@search_within_ir');

	Route::get('broker/latest', 'Front\BrokerReportController@latest');
	Route::get('broker/search-with-in-broker', 'Front\BrokerReportController@search_with');

	Route::get('funds/index-funds', 'Front\FundsController@index_funds');
	Route::get('funds/active-funds', 'Front\FundsController@active_funds');

	Route::get('pro', 'Front\ProController@index');

	Route::get('marketplace', 'Front\MarketPlaceController@index');
	Route::get('marketplace/information/{id}', 'Front\MarketPlaceController@information')->name('marketplace.information');
	Route::get('marketplace/getpayment/{id}', 'Front\MarketPlaceController@getPayment');
	Route::get('marketplace/confirm-payment/{id}', 'Front\MarketPlaceController@payments');
	Route::get('marketplace/subscribe', 'Front\MarketPlaceController@subscribe')->name('marketplace.subscribe');
	Route::get('marketplace/subscribe/wait/{id}', 'Front\MarketPlaceController@subscribe_message')->name('marketplace.subscribe.wait');
	Route::get('marketplace/pick-article/{id}', 'Front\MarketPlaceController@pick_article');
	Route::get('marketplace/limit-pick-article/{id}/{ids}', 'Front\MarketPlaceController@limit_pick_article');

	Route::any('member/mail/register', 'Member\RegisterController@mail_register')->name('member.mail.register');
	Route::any('member/register/{token?}', 'Member\RegisterController@member_register')->name('member.register');
	Route::get('member/register/welcome/{token?}', 'Member\RegisterController@welcome')->name('member.welcome');

	Route::get('account/home', 'Member\HomeController@home')->name('member.home');
	Route::get('account/feed', 'Member\HomeController@feed');
	Route::get('account/profile', 'Member\ProfileController@comments');
	Route::get('account/profile/followers', 'Member\ProfileController@followers');
	Route::get('account/profile/following', 'Member\ProfileController@following');
	Route::get('account/profile/comments', 'Member\ProfileController@comments');
	Route::get('account/profile/stocktalks', 'Member\ProfileController@stocktalks');
	Route::get('account/profile/edit/{id}', 'Member\ProfileController@edit');
	Route::get('account/latest', 'Member\HomeController@latest');
	Route::get('account/articles', 'Member\HomeController@articles');
	Route::get('account/paid-articles', 'Member\HomeController@paid_articles');
	Route::get('account/news', 'Member\HomeController@news');
	Route::get('account/data', 'Member\HomeController@data');
	Route::get('account/alert', 'Member\HomeController@alert');
	Route::get('account/stock-talks', 'Member\HomeController@stock_talks');
	Route::get('account/became-author', 'Member\HomeController@became_author');
	Route::any('account/interest/article/{slug}', 'Member\HomeController@article_detail')->name('account.interest.article.detail');
	Route::any('account/paid/article/{slug}', 'Member\HomeController@article_detail_paid')->name('account.paid.article.detail');
	Route::get('account/manage-article', 'Member\ArticleController@manage_article');

	Route::any('account/sign-in', 'Author\DashboardController@login')->name('author.login');
	Route::post('logout', 'Author\LogoutController@logout')->name('user.logout');

	Route::group(['middleware'=>'author'], function() {
		Route::get('authors/', 'Author\DashboardController@index')->name('author.home');
		Route::any('authors/write-article', 'Author\ArticleController@write_article');
		Route::get('authors/manage-article', 'Author\ArticleController@manage_article')->name('author.manage.article');
		Route::get('authors/manage-article/pending', 'Author\ArticleController@pending_article');
		Route::get('authors/manage-article/published', 'Author\ArticleController@published_article');
		Route::get('authors/manage-article/blog-post', 'Author\ArticleController@blogpost_article');
		Route::any('authors/mail-box', 'Author\MailboxController@index');
		Route::any('authors/profile', 'Author\ProfileController@index');
		Route::any('authors/manage-profile/interest', 'Author\ProfileController@ShowInterest')->name('author.profileinterest');
		Route::any('authors/manage-profile/personal-detail', 'Author\ProfileController@ShowPersonalDetail')->name('author.profilepersonal');

		Route::get('authors/manage-article/all', 'Author\ArticleController@article_all');
		Route::get('authors/manage-article/market-news', 'Author\ArticleController@article_marketNews');
		Route::get('authors/manage-article/market-outlook', 'Author\ArticleController@article_marketOutlook');
		Route::get('authors/manage-article/stockideas', 'Author\ArticleController@article_StockIdeas');
		Route::get('authors/manage-article/IR-presentation', 'Author\ArticleController@article_irPresentation');
		Route::get('authors/manage-article/broker-report', 'Author\ArticleController@article_brokerReport');
		Route::get('authors/manage-article/funds', 'Author\ArticleController@article_funds');
		Route::get('authors/manage-article-comments', 'Author\ArticleCommentController@index');
		Route::get('authors/article/{slug}', 'Author\ArticleController@article_detail')->name('author.article.detail');
		Route::any('authors/article/{slug}/edit', 'Author\ArticleController@article_edit')->name('author.article.edit');

		Route::post('upload_image', 'Author\ArticleController@upload_image')->name('article.upload.image');

	});

	Route::any('authors/mail/register', 'Author\RegisterController@mail_register')->name('author.mail.register');
	Route::any('authors/register/{token?}', 'Author\RegisterController@author_register')->name('author.register');
	Route::get('authors/register/welcome/{token?}', 'Author\RegisterController@welcome')->name('author.welcome');

	Route::get('authors/profile/{id}', 'Author\ProfileController@profiles')->name('author.profile');

	Route::post('/strEditorUpdate', 'Admin\ArticleRequestController@write_article');


	Route::any('article/{slug}', 'Front\HomeController@article_detail')->name('article.detail');

	Route::get('decrypts', function(){

		$encrypt = "eyJpdiI6IjdmWEdZOHpBUlNobE1KTGtRWlJIaEE9PSIsInZhbHVlIjoiRlBGZk54a1RycWpjb3ZZOG9HVktMQT09IiwibWFjIjoiZDUwOTA5YzIyYmM5YjM0MWEzZTcyMzAxYTVmYjNhZThlYjMxYWRmMjE3MmJjMzI5YzI4ZWEyMWM2M2M4MmU3MSJ9";

		dd(decrypt($encrypt));
	});

	Route::get('encrypt', function(){
		dd(encrypt(24));
	});

	Route::get('timestring', function(){
		$dt = Carbon\Carbon::now();

		dd($dt->format('c'));
	});
