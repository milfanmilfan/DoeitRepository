//Picture
function showPreview(file)
{
    for(var i = 0; i < file.files.length; i++){
        var each_file = file.files[i];
        var reader = new FileReader();

        reader.onload = function(e){
            //$('.show-images').append('<img src="'+e.target.result+'" />');
            $(".widget-image").attr('src', e.target.result);
        }

        reader.readAsDataURL(each_file);
    }
}