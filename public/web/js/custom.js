function validate_script($key, $message)
{
    if($key == 'identity')
    {
        $(".identity-validate").html('Pilih minimal satu');
    }

    if($("input[name='"+$key+"']").length)
    {
        $input      = $("input[name='"+$key+"']");
        $input_div  = $("input[name='"+$key+"']").parent();
        $form_input = $input.closest('.iconic-input');
        
        $input.addClass('this-error')
        $form_input.addClass('has-error');
        $input_div.append('<span class="help-block error-help-block text-left">'+$message+'</span>');
    }
    else if($("select[name='"+$key+"']").length)
    {
        $input      = $("select[name='"+$key+"']");
        $input_div  = $("select[name='"+$key+"']").parent();
        $form_input = $input.closest('.iconic-input');

        $form_input.addClass('has-error');
        $input_div.append('<span class="help-block error-help-block">'+$message+'</span>');
    }
    else if($("textarea[name='"+$key+"']").length)
    {
        $input      = $("textarea[name='"+$key+"']");
        $input_div  = $("textarea[name='"+$key+"']").parent();
        $form_input = $input.closest('.iconic-input');

        $input.addClass('this-error')
        $form_input.addClass('has-error');
        $input_div.append('<span class="help-block error-help-block text-left">'+$message+'</span>');
    }
}

function old_script($key, $value)
{
    if($key != '_token' && $key != '_method')
    {
        if($("input[name='"+$key+"']").length)
        {
            $input = $("input[name='"+$key+"']");
            $input.val($value);
        }
        else if($("select[name='"+$key+"']").length)
        {
            $input = $("select[name='"+$key+"']");

            $($input).find('option').each(function(){
    
                var value_opt = $(this).val();

                if(value_opt == $value){
                    $(this).prop('selected', 'selected');
                }
            });
        }
        else if($("textarea[name='"+$key+"']").length)
        {
            $input = $("textarea[name='"+$key+"']");
            $input.val($value);
        }
    }
}

(function($)
{
             $.fn.era_numeric = function(jenis)
     {
        if (typeof jenis === "undefined" || jenis === null) {
            jenis = false;
        }
                var type_numeric = this.attr('era-numeric');
                if( type_numeric == "true" ) {
                    jenis = true;
                }
        if(jenis == false)
        {
            this.on("keypress keyup blur", function(event){
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ($(this).val().length == 0 && event.which == 48 ){
                    event.preventDefault();
                }
                if(event.which == 8)
                {
                    return true;
                }
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        }
        else if(jenis == true)
        {
            this.on("keypress keyup blur", function(event){
                
                if(event.which == 46)
                {
                    return true;
                }
                if(event.which == 8)
                {
                    return true;
                }
                if(event.which == 0)
                {
                    return true;
                }
                if(event.which == 114)
                {
                    return true;
                }
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

        }
     };
})(jQuery);
$(function () {
        $("[era-numeric = 'true']").era_numeric();
});